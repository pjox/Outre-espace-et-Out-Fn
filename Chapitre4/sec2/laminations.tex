\begin{defn}
	Soit $\mathcal{K} = (K,\mathcal{A})$ un système d'isométries sur un arbre compact $K$. On dit que $w \in F_\mathcal{A}$ est \emph{admissible}\index{mot admissible} s'il est non-vide (en tant qu'isométrie partielle).
\end{defn}

\begin{exmp}
	$a,b,aa$ et $a^n$ avec
	\[
		|n| < \frac{1}{\alpha},
	\]
	sont des mots admissibles.
\end{exmp}

\begin{rem}
	L'ensemble des mots admissibles est clos par (sous-net) facteur. Si $w$ admissible et $w = u_1.u_2.u_3$, alors $u_1$, $u_2$ et $u_3$ sont aussi admissibles. L'ensemble
	\[
		\mathcal{L}_\text{adm} \deq \{w \in F_A \mid w \text{ admissible} \},
	\]
	est clos par sous-mot et par inverse. $\mathcal{L}_\text{adm}$ est un langage laminaire.
\end{rem}

\begin{defn}
	Soit $X$ un mot infini réduit en $\mathcal{A}^{\pm 1}$ (c'est-à-dire, $X \in \partial F_\mathcal{A}$) est \emph{admissible} \index{mot infini admissible} si tous ses préfixes le sont.
	\[
		\dom X \deq \bigcap_{n \in \nat} X_n
	\]
	est un sous-arbre fermé non-vide de $K$.
\end{defn}

\begin{defn}
	Soit $Z = X^{-1}.Y$ avec $X,Y \in  \partial F_\mathcal{A}$, un mot biinfini réduit, $Z$ est \emph{admissible} \index{mot biinfini admissible} si tous ses sous-mots le sont. En d'autres termes, $Z$ est admissible si et seulement si $X$ et $Y$ sont admissibles.
	\[
		\dom Z \deq \dom X \cap \dom Y \neq \emptyset.
	\]
\end{defn}

\begin{thm}[Gaboriau]
	Le système d'isométries $\mathcal{K} = (K, \mathcal{A})$ est à générateurs indépendants.
\end{thm}

\begin{prop}
	Les deux conditions suivantes sont équivalentes.
	\begin{enumerate}
		\item[(i)] Si $X \in \partial F_\mathcal{A}$ admissible, alors $\dom X$ est un singleton.
		\item[(ii)] Pour tout $w \in F_\mathcal{A}$ tel que $w \neq 1$, $\#(\Fix(w)) \le 1$.
	\end{enumerate}
\end{prop}

\begin{proof}
	\
	
	$\cdot$(i)$\Longrightarrow$(ii): Pour $w \neq 1$, $\#(\Fix(w)) \ge 2$.
	\[
		\Fix(w) \subseteq \Fix(w^n) \subseteq \dom w^n,
	\]
	soit
	\[
		X = w^{+\infty} = \lim_{n \to +\infty} w^n.
	\]
	Alors $\Fix(w) \subseteq \dom X$, ce qui contredit (i).
	
	$\cdot$(ii)$\Longrightarrow$(i): Par contraposé, $X \in \partial F_\mathcal{A}$, $P\neq Q \in \dom X$, par compacité\begin{align*}
		P.X_{\varphi(n)} &\longrightarrow P' \in K\\
		Q.X_{\varphi(n)} &\longrightarrow Q' \in K.
	\end{align*}
	Pour tout $\varepsilon > 0$, existe $N$ tel que pour tout $n \ge N$, $d(P,P.X_{\varphi(n)}) \le \varepsilon$ et $d(Q,Q.X_{\varphi(n)}) \le \varepsilon$.
	\begin{center}
		\begin{tikzpicture}
			\draw (0,0)--(10,0)
			(-2,1)--(0,0)
			(9,0)--(11,1)
			(10,0)--(11,-1)
			(1,0.1)--(-1,-1);
			
			\filldraw (0,0) circle (1.5 pt) node [above] {$P'$}
			(3,0) circle (1.5 pt) node [above] {$P'''$}
			(5,0) circle (1.5 pt) node [above] {$Q'''$}
			(8,0) circle (1.5 pt) node [above] {$Q''$}
			(-2,1) circle (1.5 pt) node [above] {$P.X_{\varphi(n+1)}$}
			(-1,-1) circle (1.5 pt) node [left] {$P.X_{\varphi(n)}$}
			(11,-1) circle (1.5 pt) node [right] {$Q.X_{\varphi(n)}$}
			(11,1) circle (1.5 pt) node [above] {$Q.X_{\varphi(n+1)}$};
			
			\draw [orange] (1,0.1)--(7,0.1);
			
			\filldraw [orange] (1,0.1) circle (1.5 pt) node [above] {$P''$}
			(7,0.1) circle (1.5 pt) node [above] {$Q''$};
			
			\draw[<->] (0,-0.1)--(1,-0.1) node [below, midway] {$\varepsilon$};
			\draw[<->] (1,-0.1)--(3,-0.1) node [below, midway] {$2\varepsilon$};
			\draw[<->] (5,-0.1)--(7,-0.1) node [below, midway] {$2\varepsilon$};
			\draw[<->] (7,-0.1)--(8,-0.1) node [below, midway] {$\varepsilon$};
			
			\draw[->-=.5, orange] (-1,-1) to[bend left] node [left, midway] {$u_n$} (-2,1);
			\draw[->-=.5, orange] (11,-1) to[bend right] node [right, midway] {$u_n$}  (11,1);
		\end{tikzpicture}
	\end{center}
	$u_n = X_{\varphi(n)}^{-1}X_{\varphi(n+1)}$, alors $\|u_n\|\le 2\epsilon$. $\Axe(u_n) \supseteq [P'',Q'']$.
	
	$u_n$ et $u_{n+1}$ ont une grande partie d'axe en commun et une petite longueur de translation, alors
	\[
		\Fix(u_n^{-1}u_{n+1}^{-1}u_n u_{n+1}) \supseteq [P'',Q''],
	\]
	donc
	\[
		u_n^{-1}u_{n+1}^{-1}u_n u_{n+1} = 1 \qquad \text{(d'après (ii))}
	\]
	(avec $\epsilon < d(P',Q')/6$), ainsi $u_n = c^{\alpha_n}$, $u_{n+1} = c^{\alpha_{n+1}}$.
	\[
		X =  X_{\varphi(N)} c^{\alpha_N} c^{\alpha_{N+1}} \ldots
	\]
	et $\|u_n\| \xrightarrow[n \to +\infty]{} 0$, alors $\|c\| = 0$, et donc $[P''',Q'''] \subseteq \Fix(c)$. Ce qui contredit (ii).
\end{proof}

\begin{defn}
	$\mathcal{K} = (K, \mathcal{A})$ a \emph{générateurs indépendants}\index{générateurs indépendants} si pour tout $x \in \partial F_\mathcal{A}$ admissible, $Q(x)$ est l'unique point du $\dom X$.
\end{defn}