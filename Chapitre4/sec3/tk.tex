\begin{defn}
	On définit \emph{l'arbre} $T_K$ \index{arbre $T_K$} comme
	\[
		T_K = F_A \times K / \sim
	\]
	tel que $(u,x)\sim (v,y)$ si et seulement si $x.u^{-1}v=y$.
\end{defn}
Pour tout $u \in F_\mathcal{A}$,
\[
	\{u\} \times K \hookrightarrow T_K
\]
on identifie $K$ et $\{1\} \times K$. $F_N \curvearrowright T_K$ par multiplication à gauche sur la $1^\text{ere}$ coordonnée.

Pour tout $x \in K$, tout $w \in F_\mathcal{A}$ et $x \in \dom w$
\[
	x.w = w^{-1}x
\]
où $x.w$ est une isométrie partielle et $w^{-1}x$ est l'action du groupe libre sur $T_K$. Ici, $x.w \in K$, $(1,xw) \in T_K$, et $x.w = (1,xw)$ où $K \subseteq T_K$. De même, $x \in K$, $(1,x) \in T_K$, et $x = (1,x)$ où $K \subseteq T_K$. Finalement $w^{-1}x = (w^{-1},x) \sim (1,xw)$.

\begin{lem}
	$T_K$ est un arbre réel et $F_N$ agit par isométries.
	\begin{center}
		\begin{tikzpicture}
			\filldraw [fill=orange!20,draw=orange]  plot coordinates{(0,0) (0,2) (3,2) (3,0)};
			
			\filldraw [fill=gray!20,draw=gray]  plot coordinates{(1,2) (1,3) (3,3) (3,2)};
			
			\filldraw [fill=gray!20,draw=gray]  plot coordinates{(3,0) (4,1) (6,1) (5,0)};
			
			\filldraw [orange] (0.4,0) circle (1.5pt) node [below, orange] {$(1,x)$};
			
			\filldraw [orange] (0.4,2) circle (1.5pt) node [above, orange] {$(a,xa)$};
			
			\filldraw [gray] (4,0) circle (1.5pt) node [below, gray] {$(1,x')$};
			
			\filldraw [gray] (5,1) circle (1.5pt) node [above, gray] {$(b,x'b)$};
			
			\draw [orange] (0.4,0)--(0.4,2);
			
			\draw [gray] (4,0)--(5,1);
			
			\draw (0,0)--(5,0)
			(-2,2)--(3,2)
			(1,3)--(6,3)
			(4,1)--(9,1);
			
			\draw (0, 0.1) -- (0,-0.1) node [left,midway] {$\{1\}\times [0,1]$}
			(-2, 2.1) -- (-2,1.9) node [left, midway] {$\{a\}\times [0,1]$}
			(1, 3.1) -- (1,2.9)
			(4, 1.1) -- (4,0.9)
			(9, 1.1) -- (9,0.9) node [right, midway] {$\{b\}\times [0,1]$}
			(6, 3.1) -- (6,2.9) node [right, midway] {$\{ab\}\times [0,1]$}
			(3, 2.1) -- (3,1.9)
			(5, 0.1) -- (5,-0.1);
		\end{tikzpicture}
	\end{center}
\end{lem}

\begin{prop}
	Soient $K',K''$ et $K'''$ trois arbre réels tels que
	\[
		\begin{tikzcd}[column sep = large, row sep = small]
			& K'' \\
			K'\ar[ru, hook]\ar[rd, hook] & \\
			& K'''
		\end{tikzcd}
	\]
	alors $K'$ est fermé et
	\[
		K'' \cup K'''/K'
	\]
	est un arbre réel.
\end{prop}

\begin{proof}
	Par récurrence, soient $u_0,u_1,\ldots,u_n,\ldots$ éléments du groupe libre où les préfixes précédent
	\[
		K_n = \bigcup_{k=1}^n \{u_k \} \times K /\sim
	\]
	est un arbre réel.
	\[
		K_{n+1} = K_n \cup \{u_{n+1} \} \times K / K'
	\]
	$u_{n+1} = v.a$, où
	\begin{align*}
		K' &= \{u_{n+1} \} \times \im a\\
		&= \{u_{n} \} \times \dom a,
	\end{align*}
	$(u_{n+1},x)\sim (u_k,y)$, alors $x \in \im a$ et $(u_{n+1},x)\sim (v,xa^{-1})$, où $xa^{-1} \in K_n$.
\end{proof}

\begin{rem}
	Un mot $w$ est admissible si et seulement si $K \cap wK = \dom w$.
\end{rem}

\begin{lem}\label{lemme1}
	Soit $w$ un mot qui n'est pas admissible, $K \cap wK = \emptyset$, alors $[K; wK]$ intersecte tous les $w_i K$ où $w_i$ est préfixe de $w$. $[K; wK]$ est le segment qui relie $k$ et $wK$ dans $T_K$.
\end{lem}

\begin{rem}
	Supposons que pour tout $a \in \mathcal{A}$, $a \neq 0$, et $\bigcup_i w_i K$ connexe, alors
	\[
		w_{i+1}K\cap w_i K = w_i \underbrace{(aK\cap K)}_{= \dom a \neq 0}.
	\]
\end{rem}

\begin{lem}
	Soit $u.v.w$ un mot réduit tel que $u$ est non-admissible. Alors $[K,uvK] = [P,Q]$ et $[K,uvwK] = [P,Q'']$ et $Q' \in [P,Q'']$.
	\begin{center}
		\begin{tikzpicture}
			\draw (0,3)--(0,0);
			\draw (0,2.5)--(1,3);
			\draw (0,1.5)--(-1,1);
			
			\draw (4,4)--(4,1);
			\draw (4,3.5)--(5,4);
			\draw (4,2.5)--(3,2);
			
			\draw (8,3)--(8,0);
			\draw (8,2.5)--(9,3);
			\draw (8,1.5)--(7,1);
			\draw (8,1)--(9,0);
			
			\draw [orange] (0,1) to[out = 0, in = 180 ] (4,3);
			\draw [orange] (4,3) to[out = 0, in = 180 ] (8,2);
			
			\filldraw[orange] (0,1) circle (1.5pt) node [left, orange] {$P$};
			\filldraw[orange] (4,3) circle (1.5pt);
			\filldraw[orange] (8,2) circle (1.5pt) node [right, orange] {$Q''$};
			
			\draw [orange] (3.7,2.9)  node [above] {$Q$};
			
			\draw (0,0) node [below] {$K$};
			\draw (4,1) node [below] {$uvK$};
			\draw (8,0) node [below] {$uvwK$};
		\end{tikzpicture}
	\end{center}
\end{lem}

\begin{proof}
	$[K; uvwK] \cap uvK \neq 0$ d'après le lemme \ref{lemme1}. $X \in \partial F\mathcal{A}$ ultimement admissible: $X = u.X'$ où $X' \in \partial F_\mathcal{A}$ est admissible
	\[
		Q(x) = u Q(X')
	\]
	ça ne dépend pas du choix de $u$ et $X'$. En effet, soit $X = u'.X''$ avec $X''$ admissible alors $X = u.v.X''$ et $X' = v.X''$. $\{Q(X') \} = \dom X'$, $\{Q(X').v\} = \dom X''$.
	\begin{align*}
		Q(X'') &= Q(X').v\\
		Q(X) = uQ&(X') = uvQ(X''),
	\end{align*}
	car $Q(X'') = v^{-1}Q(X')$ si et seulement si $Q(X') = vQ(X'')$. Si $X$ n'est pas ultimement admissible
	\begin{align*}
		[K; X_nK] &= [P; Q_n]\\
		[P; Q_n] &\subseteq [P; Q_{n+1}],
	\end{align*}
	et
	\[
		Q(X) = \lim_{n \to +\infty} Q_n.
	\]
\end{proof}

\begin{rem}
	Soit $X$ admissible, alors il existe $(X_{\varphi(n)})_{n\in \nat}$ un suite de préfixes de $X$, et un point $P \in K$
	\[
		\lim_{n \to +\infty} X_{\varphi(n)}^{-1} P = Q(X).
	\]
	Prenons $Q \in \dom X$, $Q X_n \in K$ et $QX_{\varphi(n)} \xrightarrow[]{n \to \infty} P$
	\[
		d(Q,X_{\varphi(n)}P) = d(X_{\varphi(n)}^{-1}Q, P) = d(Q X_{\varphi(n)}, P) \xrightarrow[]{n \to \infty} 0.
	\]
	$X_{\varphi(n)}P \in T_K$, mais pas forcement dans $K$.
\end{rem}

\begin{rapp}
	Soit $F_N \curvearrowright T$ minimale, très petite et à orbites denses, alors il existe $Q: \partial F_N \to \hat{T}$ et pour tout $X \in \partial F_\mathcal{A}$, tout $u_n \in F_\mathcal{A}$ et tout $P \in \overline{T}$; si $u_n \xrightarrow[]{n \to \infty} X$ et si $u_n P \xrightarrow[]{n \to \infty} Q$, alors $Q(X) = Q$.
\end{rapp}

\begin{lem}
	Soit $(K, \mathcal{A})$ à générateurs indépendants, alors $T_K$ a des stabilisateurs d'arcs triviaux.
\end{lem}

\begin{proof}
	Soit $w$ cycliquement réduit alors, soit $\Axe(w) \cap K \neq \emptyset$, soit $\Fix(w) \subseteq K$. En effet si $w$ est admissible existe $P \in \dom w$, $[P; Pw] \subseteq K$ et $\Axe(w) \cap [P; Pw] \neq \emptyset$.
	
	Si $w$ n'est pas admissible et cycliquement réduit. $[K; wK]$ et $[wK; w^2 K]$.
	
	\begin{center}
		\begin{tikzpicture}
		\draw (0,3)--(0,0);
		\draw (0,2.5)--(1,3);
		\draw (0,1.5)--(-1,1);
		
		\draw (4,4)--(4,1);
		\draw (4,3.5)--(5,4);
		\draw (4,2.5)--(3,2);
		
		\draw (8,3)--(8,0);
		\draw (8,2.5)--(9,3);
		\draw (8,1.5)--(7,1);
		\draw (8,1)--(9,0);
		
		\draw [orange] (0,1) to[out = 0, in = 180 ] (4,3);
		\draw [orange] (4,3) to[out = 0, in = 180 ] (8,2);
		
		\filldraw[orange] (0,1) circle (1.5pt) node [left, orange] {$P$};
		\filldraw[orange] (4,3) circle (1.5pt);
		\filldraw[orange] (8,2) circle (1.5pt) node [right, orange] {$wP$};
		
		\draw [orange] (3.55,2.9)  node [above] {$w^2 P$};
		
		\draw (0,0) node [below] {$K$};
		\draw (4,1) node [below] {$wK$};
		\draw (8,0) node [below] {$w^2K$};
		\end{tikzpicture}
	\end{center}
	Alors $wP \in [P; w^2 P]$, ainsi $P \in \Axe(w)$ et $\|w\| > 0$. ($P,wP, w^2 P \in \Axe(w)$ donc $\|w\| = d(P, wP)$ et $K \cap wK = \emptyset$).
\end{proof}

\begin{rem}
	Pour $w \in F_\mathcal{A}$, quitte à conjugueur, il est cycliquement réduit
	\[
		\underbrace{\Fix(w)}_{\mathclap{\substack{
					\text{Isométrie}\\
				\text{partielle}}}} =\underbrace{\Fix(w)}_{\mathclap{\substack{
				\text{Action}\\
				\text{du groupe}}}}.
	\]
\end{rem}

\begin{lem}
	Soit $(K,\mathcal{A})$ tel que pour tout $Q \in K$, existe $X \in \partial F_\mathcal{A}$ admissible tel que $Q(X) = Q$. Alors $T_K$ est à orbites denses.
\end{lem}

\begin{rem}
	$T_K$ n'est pas minimal en général car $K$ peut contenir des points extrémaux de $T_K$. Moins $\mathring{T}_K$ est minimal à orbites denses.
\end{rem}