\begin{defn}
	Soit $\mathcal{A}$ une base de $F_N$, alors
	\[
		F_N \cong F(\mathcal{A}) = \{\text{mots finis réduits sur } \mathcal{A}^{\pm 1} \}
	\]
	Nous disons que $L \subset F(\mathcal{A})$ est un \emph{langage laminaire}\index{langage laminaire} si
	\begin{enumerate}
		\item Il est invariant par inversion, c'est-à-dire, pour tout $u \in L$ on a $u^{-1} \in L$.
		\item Il est invariant par sous-mots, c'est-à-dire, si $u \in L$ et $p,v,s \in F(\mathcal{A})$ tels que $p.v.s = u$, alors $v \in L$.
		\item Il est biextensible, c'est-à-dire, que pour tout $u \in L$ il existe un $w \in L$ tel que $w = p.u.s$ avec $p,s \in F(\mathcal{A}) - \{1\}$.
	\end{enumerate}
\end{defn}

\begin{defn}
	On définit
	\[
		L^a \deq \{Z \in \Sigma_{\mathcal{A}} \; | \; \text{tous les sous-mots de } Z \text{ sont dans } L \}.
	\]
	Notons que $L^a$ est une lamination symbolique. On définit aussi
	\[
		\mathcal{L}(L^a) \deq \{u \in F(\mathcal{A}) \; | \; \text{il exsite } z \in L^a \text{ tel que } u < z \}.
	\]
\end{defn}

On a quelques propriétés pour les langages laminaires, à savoir
\begin{enumerate}
	\item Un langage laminaire définit une lamination symbolique $L^a$.
	\item Une lamination symbolique définit un langage laminaire $\mathcal{L} (L^a)$.
\end{enumerate}

\begin{rem}
	Soit $S \subset F(\mathcal{A})$ infini, tel que $S = \{w_1, w_2, \ldots, w_k,\ldots \}_{k \in \inte}$. On peut considérer $B = \{Z \in \Sigma_{\mathcal{A}} \; | \; \text{tous les sous-mots de } Z \text{ sont dans } S \} \neq \emptyset$ et on prend $L^a$ comme la plus petite lamination qui contient $B$.
\end{rem}

\begin{rem}
	On peut mettre une topologie métrisable sur l'ensemble des laminations algébriques, symboliques et des langages laminaires.
	
	Par exemple en utilisant les langages laminaires, on peut prendre $L$ un langage laminaire, $L(n) = \{u \in L \; | \; |u|\le n \}$ et on peut définir une distance
	\[
		d(L,L') = e^{-\max\{n\; | \; L(n)=L'(n) \}},
	\]
	et on transporte la topologie aux autres catégories via les bijections naturels.
\end{rem}

Maintenant, on rappelle
\begin{thm}[Borne de simplification de Cooper]
	Soit $\varphi \in \Aut(F_N)$, alors il existe $C>0$ et $u,v,w \in F(A)$ tels que $w = uv$ et la simplification entre $\varphi(u)$ et $\varphi(v)$ est au plus de longueur $C$.
\end{thm}

\begin{center}
	\begin{tikzpicture}
		\filldraw (-4,0) circle (1.5pt) node [left] {$w=$}
		(-2,0) circle (1.5pt)
		(0,0) circle (1.5pt)
		(4,0) circle (1.5pt)
		(6,0) circle (1.5pt)
		(6,1.5) circle (1.5pt)
		(8,0) circle (1.5pt);
		
		\draw[->-=.5] (-4,0)--(-2,0) node [above,midway] {$u$};
		\draw[->-=.5] (-2,0)--(0,0) node [above,midway] {$v$};
		
		\draw[->] (1,0) .. controls (1.5,0.25) and (2.5,0.25) .. node [above,midway] {$\varphi$} (3,0);
		
		\draw[->-=.5] (4,0)--(6,0) node [below,midway] {$\varphi(u)$};
		\draw[->-=.5] (6,0)--(8,0) node [below,midway] {$\varphi(v)$};
		\draw (6,0)--(6,1.5);
		\draw[<->, draw = red] (6.25,0)--(6.25,1.5) node [right, midway] {$C$};
	\end{tikzpicture}
\end{center}

\begin{rem}
	Soient $\varphi, \varphi' \in \Phi \in \Out(F_N)$ tels que $\varphi' = i_g \circ \varphi$ et $\varphi'(a) = g\varphi(a) g^{-1}.$
	\begin{center}
		\begin{tikzpicture}
			\filldraw (-4,0) circle (1.5pt)
			(-2,0) circle (1.5pt)
			(0,0) circle (1.5pt)
			(4,0) circle (1.5pt)
			(6,0) circle (1.5pt)
			(6,1.5) circle (1.5pt)
			(8,0) circle (1.5pt);
			
			\draw[->-=.5] (-4,0)--(-2,0) node [above,midway] {$u$};
			\draw[->-=.5] (-2,0)--(0,0) node [above,midway] {$v$};
			
			\draw[->] (1,0) .. controls (1.5,0.25) and (2.5,0.25) .. node [above,midway] {$\varphi'$} (3,0);
			
			\draw[->-=.5] (4,0)--(5,0) node [above,midway] {$g$};
			\draw[->-=.5] (5,0)--(6,0) node [below,midway] {$\varphi(u)$};
			\draw[->-=.5] (6,0)--(7,0) node [below,midway] {$\varphi(v)$};
			\draw[->-=.5] (8,0)--(7,0) node [above,midway] {$g$};
			\draw[->-=.5] (6,1.5)--(6,0) node [right,midway] {$g$};
			
			\filldraw (-4,3) circle (1.5pt)
			(-2,3) circle (1.5pt)
			(0,3) circle (1.5pt)
			(4,3) circle (1.5pt)
			(6,3) circle (1.5pt)
			(8,3) circle (1.5pt);
			
			\draw[->-=.5] (-4,3)--(-2,3) node [above,midway] {$u$};
			\draw[->-=.5] (-2,3)--(0,3) node [above,midway] {$v$};
			
			\draw[->] (1,3) .. controls (1.5,3.25) and (2.5,3.25) .. node [above,midway] {$\varphi$} (3,3);
			
			\draw[->-=.5] (4,3)--(6,3) node [below,midway] {$\varphi(u)$};
			\draw[->-=.5] (6,3)--(8,3) node [below,midway] {$\varphi(v)$};
		\end{tikzpicture}
	\end{center}
	Alors, la borne de Cooper dépend de $\varphi \in \Aut(F_N)$ et pas de $\Phi \in \Out(F_N)$.
\end{rem}

Soit $ \Phi \in \Out(F_N)$ et $L$ un langage laminaire, notre but maintenant sera de définir $\Phi(L)$.

\begin{nota}
	Soit $u \in F(A)$ et  $C > 0$, nous notons
	\[
		u_{T_C} \deq \text{ on retire à } u \text{ son préfixe et son suffixe de longueur } C
	\]
	S'il ne reste rien après avoir enlevé le suffixe et le préfixe, on note $u_{T_C} = 1$.
\end{nota}

\begin{defn}
	Soit $ \Phi \in \Out(F_N)$ et $L$ un langage laminaire, on définit alors
	\[
		\Phi(L) \deq \text{ saturer par sous-mots l'ensemble } \{\varphi_{T_C}(u) \; | \; u \in L \}.
	\]
\end{defn}

\begin{prop}
	Soit $ \Phi \in \Out(F_N)$ et $L$ un langage laminaire, l'application $\Phi(L)$ définit une action de $\Out(F_N)$ sur l'espace des langages laminaires.
\end{prop}

D'après la proposition, on obtient une action de $\Out(F_N)$ sur les laminations algébriques. Soit $\Phi \in \Out(F_N)$ et $\varphi \in \Aut(F_N)$ tel que $\varphi \in \Phi$; $\varphi$ est une isométrie de $F_N$, donc elle induit un homéomorphisme
\[
	\partial\varphi: \partial F_N \longrightarrow \partial F_N.
\]
Soit $L \subset \partial^2 F_N$ une lamination algébrique, alors $\Phi(L) = \{(\partial \varphi(x), \partial\varphi(y)) \; | \; (x,y) \in L \}$. Nous avons
\[
	(g\partial \varphi(x), g\partial\varphi(y)) = g(\partial \varphi(x), \partial\varphi(y)),
\]
car $g = \varphi(\varphi^{-1}(g))$.

\begin{center}
	\begin{tikzpicture}
		\draw l-system [l-system={cayleys, axiom=[F] [+F] [-F] [++F], step=2cm, order=2}];
		\draw[line width=0.3mm, orange] (0,0)--(0,2);
		\draw[line width=0.3mm, orange] (-1,2)--(1,2);
		\draw[->, line width=0.3mm, orange] (-1,2)--(-1,1.5) node [below] {$x$};
		\draw[->, line width=0.3mm, orange] (1,2)--(1,2.5) node [above] {$y$};
		
		\draw[line width=0.3mm, blue] (0,0)--(2,0);
		\draw[line width=0.3mm, blue] (2,-1)--(2,1);
		\draw[->, line width=0.3mm, blue] (2,1)--(1.5,1) node [left] {$\partial \varphi(x)$};
		\draw[->, line width=0.3mm, blue] (2,-1)--(2.5,-1) node [right] {$\partial \varphi(y)$};
		
		\draw[->] (1,3.5) .. controls (2.25,3) and (3,2.25) .. node [midway, right] {$\partial \varphi$} (3.5,1);
	\end{tikzpicture}
\end{center}

\begin{rem}
	Soit $\varphi' = i_g \circ \varphi$ et $\partial\varphi'(x) = g \partial \varphi(x)$. Alors
	\begin{align*}
		\{(\partial \varphi'(x), \partial\varphi'(y)) \; | \; (x,y) \in L \} &= \{g(\partial \varphi(x), \partial\varphi(y)) \; | \; (x,y) \in L \}\\
		&= g\cdot \Phi(L)\\
		&= \Phi(L),
	\end{align*}
	car $\Phi(L)$ est $F_N-$invariante.
\end{rem}

$\Out(F_N)$ agit aussi sur les laminations symboliques. Soit $L \subset \Sigma_{\mathcal{A}}$ et prenons $Z = Z_-^{-1}.Z_+$, dont $Z_+ = z_1z_2\cdots z_k\cdots$ et $Z_- = z_0^{-1}z_{-1}^{-1}\cdots z_{-k}^{-1} \cdots$. Notons que $Z_+, Z_- \in \partial F(A)$, et $\partial \varphi(Z_+),\partial\varphi( Z_-) \in \partial F(A)$.

\begin{center}
	\begin{tikzpicture}
	\draw l-system [l-system={cayleys, axiom=[F] [+F] [-F] [++F], step=2cm, order=2}];
	\draw[line width=0.3mm, orange] (-2,0)--(2,0);
	\draw[line width=0.3mm, orange] (-2,0)--(-2,1);
	\draw[line width=0.3mm, orange] (2,0)--(2,-1);
	\draw[->, line width=0.3mm, orange] (-2,1)--(-2.5,1) node [left] {$Z_-$};
	\draw[->, line width=0.3mm, orange] (2,-1)--(2.5,-1) node [right] {$Z_+$};
	
	\draw[line width=0.3mm, blue] (0,0)--(0,2);
	\draw[line width=0.3mm, blue] (0,2)--(1,2);
	\draw[->, line width=0.3mm, blue] (1,2)--(1,1.5) node [near end,right] {$\partial \varphi(Z_+)$};
	\draw[->, line width=0.3mm, blue] (1,2)--(1,2.5) node [near end,right] {$\partial \varphi(Z_-)$};
	\end{tikzpicture}
\end{center}

Ici, on a $\varphi(Z) = \partial \varphi(Z_-)^{-1}.\partial \varphi(Z_+)$, et on simplifie si c'est nécessaire, où l'indice $0$ est juste avant la simplification.

Finalement, on prend $\Phi(L) = \{\sigma^k\varphi(Z) \; | \; Z \in L, k \in \inte \}$.
