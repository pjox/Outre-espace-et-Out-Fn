On commence par rappeler quelques définitions importantes de topologie général

\begin{defn}
	Soit $X$ un espace topologique, nous disons que $X$ est \emph{parfait}\index{espace topologique parfait} si $X$ ne contient pas des points isolés.
\end{defn}

\begin{defn}
	Soit $X$ un espace topologique, nous disons que $X$ est \emph{totalement discontinu}\index{espace topologique totalement discontinu} si la composante connexe de tout point $x$ de $X$ est le singleton $\{x\}$.
\end{defn}

\begin{defn}
	Soit $A_0$ l'intervalle $[0,1] \subseteq \real$. Soit $A_1$ l'ensemble obtenu de $A_0$ en enlevant le sous-intervalle $\left(\frac{1}{3}, \frac{2}{3} \right)$. Soit $A_2$ l'ensemble obtenu de $A_1$ en enlevant les sous-intervalles $\left(\frac{1}{9}, \frac{2}{9} \right)$ et $\left(\frac{7}{9}, \frac{8}{9} \right)$. En général, définissez $A_n$ par l'équation
	\[
		A_n = A_{n-1} - \bigcup_{k=0}^\infty \left(\frac{1+3k}{3^n},\frac{2+3k}{3^n} \right).
	\]
	Nous appelons l'intersection
	\[
		C = \bigcap_{n=0}^\infty A_n
	\]
	l'\emph{ensemble triadique de Cantor}\index{ensemble triadique de Cantor}.
\end{defn}

\begin{rem}
	L'ensemble triadique de Cantor est un espace topologique compact, métrisable, parfait et totalement discontinu.
\end{rem}

\begin{defn}
	Soit $X$ un espace topologique, nous disons que $X$ est un \emph{espace de Cantor}\index{espace de Cantor} si $X$ est homeomorphe à l'espace triadique de Cantor.
\end{defn}

Maintenant, considérons $F_N$ le groupe libre de rang $N$ et prenons $\partial F_N$ le bord de Gromov; muni d'une topologie, $\partial F_N$ est un espace de Cantor.

Soit $g \in F_n$, $\partial F_N$ est muni d'une action de $F_N$, donné par
\begin{align*}
	g:\partial F_N &\longrightarrow \partial F_N\\
	X &\longmapsto gX,
\end{align*}
la concaténation ( et réduction ) de $g$ et $X$. $g \in F_N$ induit un homéomorphisme qui a toujours deux points fixes, à savoir, $g^{-\infty}$ et $g^{\infty}$ c'est-à-dire $g^{-1}g^{-1}g^{-1}\ldots$ et $ggg\ldots$ pour tout $g \neq 1$.

\begin{defn}
	Nous appelons les \emph{points rationnels}\index{points rationnels} de $\partial F_N$ tous les points de la forme $g^{\infty}$ pour un $g \in F_N$ tel que $g \neq 1$.
\end{defn}

\begin{rem}
	L'ensemble des points rationnels de $\partial F_N$ est un ensemble dense dans $\partial F_N$.
\end{rem}

\begin{defn}
	Soit $F_N$ le groupe libre de rang $N$, on définit le \emph{bord double}\index{bord double} de $F_N$ comme
	\[
		\partial^2 F_N \deq \partial F_N \times \partial F_N - \Delta,
	\]
	où $\Delta = \{(x,x) \, | \, x \in \partial F_N \}$. $\partial^2 F_N$ est muni d'une topologie ( topologie produit ) induite par celle de $\partial F_N$, et il est muni aussi d'une action diagonal induite par celle sur $\partial F_N$
	\begin{align*}
		g:\partial^2 F_N &\longrightarrow \partial^2 F_N\\
		(X,Y) &\longmapsto (gX,gY).
	\end{align*}
	Finalement, $\partial F_N$ est muni d'une autre action qu'on appellera le \emph{flip}\index{flip}, donné par
	\begin{align*}
		\partial^2 F_N &\longrightarrow \partial^2 F_N\\
		(X,Y) &\longmapsto (Y,X).
	\end{align*}
\end{defn}

\begin{rem}
	$\partial^2 F_N$ n'est pas compact. En effet, soit $g \in F_N$, alors $g^k\cdot (X,Y) = (g^k X, g^k Y)$ et la limite
	\[
		\lim_{k \to \infty} g^k\cdot (X,Y) = (g^\infty,g^\infty).
	\]
	Cependant, $(g^\infty,g^\infty) \notin \partial^2 F_N$.
\end{rem}

\begin{defn}
	Une lamination algébrique est un sous-ensemble $\Lambda \subseteq \partial^2 F_N$ qui est fermé, $F_N$-invariant et flip-invariant.
\end{defn}

\begin{exmp}\label{rationelle}
	Soit $g \in F_N$ tel que $g \neq 1$, et soit $(g^{-\infty},g^\infty) \in \partial^2 F_N$. La plus petite lamination qui contient $(g^{-\infty},g^\infty)$ est $L(g) \deq F_N\cdot \left( (g^{-\infty},g^\infty) \sqcup (g^{\infty},g^{-\infty}) \right)$.
\end{exmp}

\begin{defn}
	On appelle une lamination comme celle de l'exemple \ref{rationelle}, une \emph{lamination rationnelle}\index{lamination rationnelle}. Notons que l'union finie de laminations est une lamination. La lamination $L(g)$ est engendré par $g$.
\end{defn}