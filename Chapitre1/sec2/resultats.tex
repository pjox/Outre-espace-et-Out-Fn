\begin{thm}[Nielsen-Schreier]
	Tout sous-groupe d'un groupe libre est libre.
\end{thm}

\begin{proof}
	Prenons un système transverse de Schreier
	\[
		\begin{tikzcd}[column sep=huge]
			F_A \ar[r,shift left=1ex] & F_A/H \ar[l,shift left=1ex]
		\end{tikzcd}
	\]
	où $F_A/H$ est une section close par préfixe; c'est-à-dire, si $\tau \subseteq F_A$ alors par tous $t,t' \in \tau$ tels que $Ht=Ht'$ nous avons $t =t'$. De plus, par tout $g \in F_A$ il existe $t \in \tau$ tel que $Hg=Ht$. Si $t \in \tau$ et $t=t'.t''$, $t'$ est un préfixe de $t$, alors $t'\in\tau$. On peut construire $\tau$ par induction, donc $\tau$ existe.
	
	Définissons $\mathcal{B}\deq\{t.a.t'^{-1} \; | \; t,t' \in \tau; a \in A \text{ et } Hta=Ht' \}$. Ici, $H$ est un groupe libre sur $\mathcal{B}$.
\end{proof}

\begin{exmp}
	Prenons $\tau$ sous-arbre maximal du graphe $F_A/H$, $\{1, b, ba^{-1}, ba^{-1}b, ba^{-1}bb, ba^{-1}$ $b^{-1} \}$. La base est donné par les arrêtes hors de $\tau$.
	\vfill
	\begin{center}
		\begin{tikzpicture}
			\filldraw (-2,0)circle (2pt)
			(0,0) circle (2pt)
			(2,0) circle (2pt) node [right] {$1$}
			(2,2) circle (2pt)
			(2,-2) circle (2pt);
			
			\draw[->-=.5,draw=orange] (0,0)--(-2,0) node [below,midway] {$b$};
			\draw[->-=.5,draw=blue] (-2,0) .. controls (-1.5,0.5) and (-0.5,0.5) .. node [above,midway] {$b$} (0,0);
			\draw[->-=.5,draw=blue] (0,0) .. controls (0.5,0.5) and (1.5,0.5) .. node [above,midway] {$a$} (2,0);
			\draw[->-=.5, draw=orange] (2,0) .. controls (1.5,-0.5) and (0.5,-0.5) .. node [below,midway] {$a$} (0,0);
			\draw[->-=.5,draw=blue] (-2,0)--(2,-2) node [below,midway] {$a$};
			\draw[->-=.5,draw=orange] (2,-2) .. controls (-0.5,-2) and (-2,-0.5) .. node [below,midway] {$a$} (-2,0);
			\draw[->-=.5,draw=orange] (2,-2)--(2,0) node [right,midway] {$b$};
			\draw[->-=.5, draw=orange] (2,0)--(2,2) node [right,midway] {$b$};
			\draw[->-=.5,draw=orange] (2,2) .. controls (3,1) and (3,-1) .. node [right,midway] {$b$} (2,-2);
			\draw[->-=.5, draw=orange] (2,2) .. controls (1,3) and (3,3) .. node [above,midway] {$a$} (2,2);
			
			\draw[->] (3,0.5) .. controls (4,1) and (5,1) .. (6,0.5);
			
			\draw[->-=.5] (8,0) .. controls (6,2) and (6,-2) .. node [left] {$b$} (8,0);
			\draw[->-=.5] (8,0) .. controls (10,2) and (10,-2) .. node [right] {$a$} (8,0);
		\end{tikzpicture}
	\end{center}
	Prenons
	\[
		H = \langle \textcolor{orange}{b}\textcolor{blue}{b},\textcolor{orange}{b}\textcolor{blue}{a}\textcolor{orange}{a}\textcolor{orange}{b^{-1}}, \textcolor{blue}{a}\textcolor{orange}{b^{-1}}\textcolor{orange}{a}\textcolor{orange}{b^{-1}},\ldots \rangle,
	\]
	alors $\mathcal{B}=\{t.a.t^{-1} \; | \; t,t' \in \tau, \text{ et } Hta=Ht' \}$ est un système réduit de Nielsen (les lettres bleues ne s'effacent jamais).
\end{exmp}

\begin{exmp}
	Le groupe fondamental d'un graphe fini connexe est un groupe libre.
\end{exmp}

\begin{exmp}
	Le sous-groupe de $SL_2(\inte)$ engendré par
	\[
		A = \begin{pmatrix}
			1 & 2\\
			0 & 1
		\end{pmatrix}
		\quad \text{ et } \quad
		B = \begin{pmatrix}
			1 & 0\\
			2 & 1
		\end{pmatrix}.
	\]
\end{exmp}

\begin{thm}
	Soit $H$ un sous-groupe de $F_A$ d'indice fini, alors
	\[
		(\rang H - 1) = (\rang F_A - 1)[F_A:H].
	\]
\end{thm}

\begin{defn}
	Soit $G$ un groupe, nous disons que $G$ est \emph{résiduellement fini}\index{résiduellement fini} si par tout $g \in G$ tel que $g \neq 1$, il existe un groupe fini $K$ et un morphisme $\varphi:G\to K$, tel que $\varphi(g) \neq 1$.
\end{defn}

\begin{prop}[Mal'cev]
	Un groupe linéaire de type fini est résidullement fini.
\end{prop}

\begin{thm}[Mal'cev]
	Les groupes libres sont résiduellement finis.
\end{thm}

\begin{proof}
	Soit $w \in F_A$ et $X = \{\text{prefixes de } w \}$. Pour $\sigma_a$ permutation de $X$ telle que si $w = u.a.v$ (occurrences de $a$ dans $w$) alors
	\[
		\sigma_a(u) = u.a,
	\]
	et si $w = u.a^{-1}.v$,
	\[
		\sigma_a (ua^{-1}) = u.
	\]
	Définissons donc
	\begin{align*}
		\varphi: F_A &\longrightarrow \Sym(X)\\
		a &\longmapsto \sigma_a,
	\end{align*}
	alors $1\cdot \varphi (w) = w$ et $w \neq 1$.
\end{proof}

\begin{defn}
	Soit $G$ un groupe, on dit que $G$ est \emph{Hopfien}\index{Hopfien} si tout endomorphisme surjectif est injectif.
\end{defn}

\begin{cor}
	Les groupes libres de type fini sont Hopfiens.
\end{cor}

\begin{proof}
	Prenons
	\[
		\varphi: F_A \twoheadrightarrow F_A
	\]
	et par l'absurde supposons qu'il existe $g \in F_A$ tel que $\varphi(g) = 1$ et $g \neq 1$. Il existent aussi $g_1, g_2, \ldots, g_n, g_{n+1}$ tels que $\varphi(g_1)=g, \varphi(g_2) =g_1,\ldots, \varphi(g_{n+1})=g_n$. Comme $F_A$ est de type fini, il existe un groupe fini $K$ et un morphisme $f:F_A \to K$ tel que $f(g) \neq 1, f\circ\varphi^n(g_{n+1})\neq 1$, mais $f\circ \varphi^n(g_k) = 1$ par tout $k \le n$.
	
	Donc les $f\circ \varphi^n$ sont deux à deux distincts, or il y a exactement $|K|^{|A|}$ morphismes de $F_A$ dans $K$. Contradiction ($k\le n-1$).
\end{proof}