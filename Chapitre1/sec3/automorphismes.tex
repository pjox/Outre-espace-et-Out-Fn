\begin{nota}
	Nous notons par $\Aut(F_N)$ le groupe de automorphismes du groupe libre.
\end{nota}

\begin{exmp}
	Prenons $F_{\{a,b,c \}}$ et considérons l'automorphisme
	\begin{align*}
		\varphi: a &\longmapsto ab\\
		b &\longmapsto ac\\
		c &\longmapsto a.
	\end{align*}
	Son inverse est donne par
	\begin{align*}
		\varphi^{-1}: a &\longmapsto c\\
		b &\longmapsto c^{-1}a\\
		c &\longmapsto c^{-1}b.
	\end{align*}
\end{exmp}

\begin{thm}[Nielsen]
	Les automorphismes élémentaires de Nielsen engendrent $\Aut(F_N)$ (et même algorithme de réduction). $\Aut(F_N)$ est de type fini et même de présentation finie.
\end{thm}

\begin{exmp}
	Soit $\Aut(F_2)$ et notons $F_{\{a,b\}}$. Pour tout morphisme $\varphi \in \Aut(F_2)$, $\varphi([a,b])$ est un conjugué de $[a,b]$ ou de son inverse $[a,b]^{-1} = [b,a]$.
\end{exmp}

\begin{rem}
	On a un morphisme surjectif
	\[
		F_N \twoheadrightarrow \inte^{N} = F_N/F'_N,
	\]
	où $F'_N$ est le sous-groupe dérivé. Donc, on a aussi
	\[
		\Aut(F_N) \twoheadrightarrow \GL_N(\inte) = \Aut(\inte^N)
	\]
	qui est surjectif car les transvexions engendrent $\GL_N(\inte)$.
\end{rem}

\begin{defn}
	Soit $w \in F_N$ nous définissons l'\emph{automorphisme intérieur}\index{automorphisme intérieur} associé à $w$ comme
	\begin{align*}
		i_w: F_N &\longrightarrow F_N\\
		u &\longmapsto i_w(u) = wuw^{-1}.
	\end{align*}
	Nous définissons aussi le \emph{sous-groupe de automorphismes intérieurs}\index{sous-groupe de automorphismes intérieurs} comme
	\[
		\Inn(F_N) \deq \{i_w \; | \; w \in F_N \}
	\]
	Notons que $\Inn(F_N)$ est un sous-groupe distingué de $\Aut(F_N)$ et que on a un morphisme naturel
	\begin{align*}
		F_N &\longrightarrow \Inn(F_N)\\
		w &\longmapsto i_w.
	\end{align*}
	De plus, on a naturellement
	\[
		\varphi \circ i_w \circ \varphi^{-1} = i_{\varphi(w)}.
	\]
	Finalement, nous définissons le \emph{groupe des automorphismes extérieurs}\index{groupe des automorphismes extérieurs} comme
	\[
		\Out(F_N) \deq \Aut(F_N)/\Inn(F_N)
	\]
\end{defn}

\begin{rem}
	Remarquons que les automorphismes intérieurs agissent trivialement sur l'abélianisé
	\[
		\begin{tikzcd}[row sep = large]
			\Aut(F_N) \ar[rr,twoheadrightarrow] \ar[dr,twoheadrightarrow] & & \GL_N(\inte)\\
			& \Out(F_N) \ar[ru,twoheadrightarrow] &
		\end{tikzcd}
	\]
\end{rem}

\begin{prop}
	$\Out(F_2) \cong \GL_2(\inte)$.
\end{prop}

Rappelons aussi que
\begin{align*}
	\PSL_2(\inte) &= \inte/2\inte \ast \inte/3\inte\\
	\SL_2(\inte) &= \inte/4\inte \ast_{\inte/2\inte} \inte/6\inte,
\end{align*}
et $\GL_2(\inte)$ contient un sous-groupe libre d'indice fini (virtuellement libre).

\begin{prop}
	Pour toute fonction $\varphi \in \Aut(F_A)$, où $A$ est de type fini, il existe $\lambda > 0$, tel que par tout $w \in F_A$, alors
	\[
		\frac{1}{\lambda}|w| \le |\varphi(w)| \le \lambda|w|.
	\]
\end{prop}

\begin{proof}
	Prenons
	\begin{align*}
		\lambda^+ &= \max \{ |\varphi(a)| \; \mid \; a \in A \},\\
		\lambda^- &= \max \{ |\varphi^{-1}(a)| \; \mid \; a \in A \}.
	\end{align*}
\end{proof}

\begin{thm}[Borne d'effacement de Cooper]
	Pour tout automorphisme $\varphi \in \Aut(F_A)$ , il existe $C$ tel que pour tous $u,v \in  F_A$, si $u.v$ est réduit, alors l'effacement entre $\varphi(u)\varphi(v)$ est majoré par $C$.
\end{thm}

\begin{proof}
	Dans ce cas $C = 1$ alors, nous avons le résultat pour les automorphismes élémentaires de Nielsen.
	
	Prenons maintenant, $C = \CCB(\varphi)$ et $D = \CCB(\psi)$, alors
	\[
		\CCB(\varphi \circ \psi) \le C + 2\lambda D,
	\]
	où $\lambda = \lambda(\varphi)$. En effet, l'effacement entre deux mots $u$ et $v$ est donné par $\frac{1}{2} \left( |u|+|v|-|uv|\right)$ qui est égal à
	\[
		\frac{1}{2} \left( |\varphi \circ \psi(u)| + |\varphi \circ \psi(v)| - |\varphi \circ \psi(uv)| \right),
	\]
	et écrivons $u' = \psi(u), v' = \psi(v)$. L'effacement entre $u'$ et $v'$ est majoré par $D$.
	\begin{align*}
		u' &= u''.w\\
		v' &= w^{-1}.v''\\
		u'v' &= u''.v''
	\end{align*}
	où $|w| \le D$. Donc
	\begin{align*}
		\frac{1}{2} \left( |\varphi \circ \psi(u)| + |\varphi \circ \psi(v)| - |\varphi \circ \psi(uv)| \right) &= \frac{1}{2} \left(|\varphi(u''w)| + |\varphi (w^{-1}.v'')| - |\varphi(u''.v'')| \right)\\ 
		&\le 	\frac{1}{2} \left(|\varphi(u'')|+ |\varphi(w)| + |\varphi(w^{-1})| \right.\\
		&\qquad \left. + |\varphi(v'')| - |\varphi(u''.v'')|\right)\\
		&\le C + 2\lambda D.
	\end{align*}
\end{proof}