\begin{defn}[Serre]
	Nous définissons un \emph{graphe}\index{graphe} $G$ comme $G = (V,E,i,t,-) $, où $V$ est l'ensemble de sommets, $E$ l'ensemble d'arrêtes, $i:E\to V$ l'application qui à chaque arrête associe son sommet initial, $t:E\to V$ l'application qui à chaque arrête associe son sommet terminal, et $-:E \to E$ une involution sans point fixes. De plus, nous exigeons que $i(\overline{e}) =t(e)$.
\end{defn}

\begin{defn}
	Un \emph{chemin}\index{chemin} $\gamma$ est une suite d'arêtes $\gamma = (e_1,e_2,\ldots,e_n)$ tel que $t(e_k) = i(e_{k+1})$, $i(\gamma) = i(e_1)$, $t(\gamma) = t(e_n)$.
	\begin{center}
		\begin{tikzpicture}
			\draw (-3,0)--(-1,0) node [above,midway] {$e_1$};
			\draw (-1,0)--(1,0) node [above,midway] {$e_2$};
			\draw (1,0)--(3,0) node [above,midway] {$e_3$};
			\filldraw (-3,0)circle (1.5pt)
			(-1,0) circle (1.5pt)
			(1,0) circle (1.5pt)
			(3,0) circle (1.5pt);
		\end{tikzpicture}
	\end{center}
	On definit aussi l'involution sur un chemin comme $\overline{\gamma} = (\overline{e_n},\overline{e_{n-1}},\ldots,\overline{e_1})$. On dit qu'un chemin est reduit si pour tout $k$, $e_{k+1}\neq \overline{e_k}$. Finalement on dit qu'un chemin est un \emph{boucle}\index{boucle} si $i(\gamma) = t(\gamma)$.
\end{defn}

\begin{defn}
	Un \emph{arbre}\index{arbre} est un graphe connexe sans cycles.
\end{defn}

\begin{defn}
	Nous définissons un morphisme de graphes
	\[
		\begin{tikzcd}[column sep = huge, row sep = tiny]
			G \ar[r,"f"] & G'\\
			E \ar[r,"f"] & E'\\
			V \ar[r,"f"] & V'
		\end{tikzcd}
	\]
	tel que $f(i(e)) = i(f(e))$, $f(t(e))=t(f(e))$ et $f(\overline{e}) = \overline{f(e)}$.
\end{defn}

\begin{defn}
	Soit $G$ un arbre, alors pour tous $u,v \in V$ on définit le \emph{segment}\index{segment} $[u,v]$ comme l'unique chemin réduit allant de $u$ à $v$ et la \emph{longueur du segment}\index{longueur du segment} comme le nombre d'arrêtes. on définit aussi la \emph{distance simplicial}\index{distance simplicial} entre $u$ et $v$ comme
	\[
		d(u,v) \deq \text{ longueur du segment } [u,v].
	\]
	En général: $d(u,v) = \min\{\rho(\gamma) \; | \; \gamma \text{ chemin de  } u \text{ à } v\}$. En plus, pour tout $t \in V$ on a que $t \in [u,v]$ si et seulement si $d(u,t)+d(t,v) = d(u,v)$.
\end{defn}

\begin{defn}
	On définit la \emph{longueur de translation}\index{longueur de translation} par $f$ comme
	\[
		\|f\|_T \deq \min \{d(v,f(v)) \; | \; v\in V\}.
	\]
\end{defn}

\begin{defn}
	Soit $f$ un automorphisme de graphes, on définit l'\emph{axe}\index{axe} de $f$ comme
	\[
		\Axe(f) \deq \{v \; | \; d(v,f(v)) = \|f\|_T \}
	\]
	les sommet minimalement déplacés.
\end{defn}

\begin{thm}
	Soit $T$ un arbre et $f:T\to T$ un automorphisme sans involution, alors
	\begin{enumerate}
		\item $f$ est elliptique si $\Fix(f)$ est un sous-arbre non-vide de $T$.
		\item $f$ est hyperbolique si $\Axe(f)$ est un sous-arbre de $T$ isomorphe à $\inte$ sur lequel $f$ agit par translation.
	\end{enumerate}
\end{thm}

\begin{proof}
	\
	
	\textbf{Premier cas:} $\Fix(f) \neq \emptyset$, alors pour tous $v,w \in \Fix(f)$, $[v,w] \subseteq \Fix(f)$. En effet, $f$ est une isométrie, donc $f([v,w]) = [f(v),f(w)]$. $\Fix(f)$ est donc un sous-ensemble convexe de $T$ et donc, il est un sous-arbre.
	
	\textbf{Deuxième cas:} Pour tout $v \in \Axe(f)$; $f(v),f^{-1}(v) \in \Axe(f)$; $d(v,f(v)) = d(f(v),f(f(v)))$ car $f$ est une isométrie.
	
	Par l'absurde, si $[v,f(v)] = (e_1,\ldots e_l)$, $[f(v),f^{2}(v)] = (e_{l+1}\ldots,e_i)$, où $e_l = \overline{e_{l+1}}$. Soit $w = i(e_l) = t(e_{l+1})$, alors $d(w,f(w)) = l-2$, contradiction. Donc $f(v) \in [v,f^2(v)]$ ou encore
	\[
		[v,f(v)] \cup [f(v),f^2(v)] = [v,f^2(v)]
	\]
	\begin{center}
		\begin{tikzpicture}
			\draw (-3,0)--(0,0);
			\draw (0,0)--(3,0);
			
			\draw (-3,0)--(-3,1.5);
			\draw (0,0)--(0,1.5) node [left,midway] {$e_l$} node [right,midway] {$e_{l+1}$};
			\draw (3,0)--(3,1.5);
			
			\filldraw (-3,1.5)circle (1.5pt) node [above] {$v$}
			(0,1.5) circle (1.5pt) node [above] {$f(v)$}
			(3,1.5) circle (1.5pt) node [above] {$f^2(v)$};
		\end{tikzpicture}
	\end{center}
	alors
	\[
		\bigcup_{n \in \inte}[f^n(v),f^{n+1}(v)]
	\]
	est isomorphe à $\inte$, de plus, pour tout $w \in [v,f(v)]$, $w \in \Axe(f)$.
\end{proof}

\begin{exmp}
	Soit $F_A$ le groupe libre sur $A$, et $T_A$ le graphe de Cayley où $V =F_A$ et $E = \{ (g,g(a)) \; | \; g \in F_A, \; a \in A^{\pm 1} \}$. Le groupe libre agit par multiplication à gauche et par isomorphismes, librement.
	Pour un mot $w \in F_A$, $\|w\|_{T_A} =$ longueur cycliquement réduite de $w$. Si $w = u.v.u^{-1}$, alors $\|w\|_A = \|v\|_A = |v|_A$ si $v.v$ est réduit.
	
	Si $w$ est cycliquement réduit. $\Axe(w) = \{v \; | \; v \text{ est un préfixe de } w^n \text{ avec } n \in \inte\}$.
	
	Si $w$ n'est pas cycliquement réduit. $\Axe(w) = u\Axe(v)$ où $w = u.v^{-1}.u$.
	
	Soient $f,g$ isométries d'un arbre. $\|f\|_T = \|gfg^{-1}\|_T$ et $\Axe(gfg^{-1}) = g\Axe(f)$.
\end{exmp}

Plus généralement $G\curvearrowright T$, le groupe $G$ agit sur l'arbre $T$ par isométries/isomorphismes sans inversions. L'action est
\begin{itemize}
	\item Minimale: s'il n'y a pas de sous-arbre propre globalement invariante.
	\item Triviale: s'il y a un sommet invariant.
	\item Libre: pour tout $g \in G$, $g \neq e$, $\Fix(g) = \emptyset$.
\end{itemize}

\begin{lem}
	Soient $f,g$ elliptiques, $\Fix(f) \cap \Fix(g) = \emptyset$ alors $g\circ f$ est hyperbolique et $\|g\circ f\|_T = 2d(\Fix(f),\Fix(f))$.
\end{lem}

\begin{lem}
	Si $v,w,h(v)$ et $h(w)$ sont alignés et distincts, alors $h$ est hyperbolique et
	\[
		\Axe(f) \supseteq [v,h(w)].
	\]
\end{lem}

\begin{center}
	\begin{tikzpicture}
		\draw (-6,0)--(6,0);
		
		\filldraw (-6,0) circle (1.5pt) node [below] {$v$}
		(-2,0) circle (1.5pt) node [below] {$w$}
		(2,0) circle (1.5pt) node [below] {$h(v)$}
		(6,0) circle (1.5pt) node [below] {$h(w)$};
	\end{tikzpicture}
\end{center}

\begin{rem}
	Si $g_1, \ldots, g_n \in G$ elliptiques pour tout $i \neq j$, $\Fix(g_i)\cap\Fix(g_j) \neq  \emptyset$ alors $\Fix(g_1)\cap \cdots \cap \Fix(g_n) \neq \emptyset$.
	
	Soit $G = \langle g_1, \ldots, g_n \rangle \curvearrowright T$ non-triviale, alors il existe un élément hyperbolique et $T_{\min}$ est connexe.
\end{rem}

\begin{lem}
	Soient $f,g$ hyperboliques tels que $\Axe(f) \cap \Axe(g) = \emptyset$, alors $f\circ g$ est hyperbolique et $\Axe(f \circ g) \cup \Axe(f) \cup \Axe(g)$ est connexe.
\end{lem}

\begin{center}
	\begin{tikzpicture}
		\draw [->-=.5] plot [smooth, tension=0.5] coordinates{(-3.5,1.5) (-2.5,1.25) (0,1.15) (2.5,1.25) (3.5,1.5)};
		
		\draw plot [smooth, tension=0.5] coordinates{(1.5,0.15) (2,0.25) (3,0.35) (4,0.25) (4.5,0.15)};
		
		\draw [->-=.5] plot [smooth, tension=0.5] coordinates{(-3.5,-1.5) (-3.25,-1.35) (-2.5,-1.25) (0,-1.15) (2.5,-1.25) (3.5,-1.5)};
		
		\draw [->-=.5,blue] plot [smooth, tension=0.5] coordinates{(-3.25,-1.35) (-2.25,-0.25) (0,0.45) (3,0.35)};
		
		\draw [line width=0.3mm, blue] plot [smooth, tension=0.5] coordinates{(-3.25,-1.35) (-2.5,-1.25)};
		
		\draw [line width=0.3mm, blue] plot [smooth, tension=0.5] coordinates{(-2.5,1.25) (0,1.15) (2.5,1.25)};
		
		\draw [blue] (-2.5,1.25)--(-2.5,-1.25);
		
		\draw [blue] (2.5,1.25)--(3,0.35);
		
		\filldraw (-3.25,-1.35) circle (1.5pt) node [below] {$g^{-1}(v)$}
		(-2.5,1.25) circle (1.5pt) node [above] {$w$}
		(2.5,1.25) circle (1.5pt) node [above] {$f(w)$}
		(-2.5,-1.25) circle (1.5pt)
		(3,0.35) circle (1.5pt) node [below] {$f\circ g(v)$};
		
		\draw (0,1.15) node [above] {$f$}
		(0,0.45) node [below] {$f\circ g$}
		(-2.15,-1.25) node [above] {$v$}
		(0,-1.15) node [below] {$g$};
	\end{tikzpicture}
\end{center}

\begin{prop}
	Supposons $G\curvearrowright T$ non-triviale et $G$ de type fini, alors il existe $T_{\min}$ un unique sous-arbre minimal invariant.
\end{prop}

\begin{proof}
	Considérons
	\[
		T_{\min} = \bigcup_{g \in G} \Axe(g) \text{ hyperbolique }
	\]
	Nous avons
	\[
		\|f \circ g \|_T = \|f\|_T + \|g\|_T + d(\Axe(f),\Axe(g)).
	\]
	De plus, pour tout $g,h \in G$, $g$ hyperbolique implique $hgh^{-1}$ hyperbolique et $h\Axe(g) = \Axe(hgh^{-1})$, donc $T_{\min}$ est un sous-arbre invariante de $T$.
	
	Remarquons de plus; $f$ hyperbolique et $v \in V$, $[v,f(v)] \cap \Axe(f) \neq \emptyset$.
	
	Si $T_0$ est un sous-arbre (non-vide) invariant, alors pour tout $v \in V(T_0)$, $Gv \subseteq T$, $f \in G$ est hyperbolique donc $[v,f(v)] \subseteq T_0$ donc $T_0$ contient un point de l'axe de $f$ et alors $\Axe(f) \subseteq T_0$ et en conséquence $T_{\min} \subseteq T_0$.
\end{proof}

