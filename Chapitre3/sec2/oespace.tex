Considérons l'espace des arbres simpliciaux avec l'action minimale libre par isomorphismes de $F_N$, dans cet espace deux arbres $T$ et $T'$ satisfont $T = T'$ si et seulement s'il existe un isomorphisme
\[
	T \xrightarrow{\quad\sim\quad} T'
\]
$F_n$ équivariant.

\begin{defn}
	Soit $\Gamma$ un graphe, on définit le \emph{core}\index{core} de $\Gamma$, $\core(\Gamma)$ comme l'union de tous les boucles réduits dans $\Gamma$.
\end{defn}

\begin{defn}
	Pour chaque base $\mathcal{A}$ de $F_N$, considérons une graphe fini connexe $\Gamma$ tel que $\pi_1(\Gamma) = F_N$. Nous disons que $\pi_1(\Gamma) \curvearrowright \widetilde{\Gamma}$ est une \emph{action minimale}\index{action minimale} si et seulement $\Gamma = \core(\Gamma)$.
	
	\begin{center}
		\begin{tikzpicture}
			\draw (-1.5,0) node [left] {$\Gamma=$};
			
			\draw plot [smooth cycle, tension=1] coordinates{(1.5,0) (0,1.5) (-1.5,0) (0,-1.5)};
			
			\draw (0,0)--(0, 1.5);
			\draw (0,0)--(-1.06, -1.06);
			\draw (0,0)--(1.06, -1.06);
			
			\filldraw (0, 1.5) circle (1.5pt)
			(-1.06, -1.06) circle (1.5pt)
			(1.06, -1.06) circle (1.5pt)
			(0, 0) circle (1.5pt);
		\end{tikzpicture}
	\end{center}
\end{defn}

Nous nous concentrerons maintenant sur les automorphismes $F_N \curvearrowright T$. Soit $\varphi \in \Aut(F_N)$. $\varphi(T)$ es un arbre avec une action tordue par $\varphi$. Soit $\alpha: F_N \to \Isom(T)$, alors $\varphi(T,\alpha) = (T,\alpha\circ\varphi^{-1})$.

Remarquons que par un automorphisme intérieur $i_w$
\begin{align*}
	i_w:F_N &\longrightarrow F_N \\
	y &\longmapsto wuw^{-1},
\end{align*}
et $i_w(T) = T$, alors $i_w(T,\alpha) = (T, \alpha \circ i^{-1}_w)$. Alors
\begin{align*}
	T &\xrightarrow{\quad\sim\quad} T\\
	x &\xmapsto{\quad\quad\quad} \alpha(w)(x)
\end{align*}
est $F_N$-équivariant:
\begin{align*}
	\alpha(u)(x) \xmapsto{\quad\quad\quad} &\alpha(w^{-1})(\alpha(u)(x))\\
	&=\alpha(w^{-1}u)(x) \\
	&= \alpha(w^{-1}uww^{-1})(x)\\
	&= \alpha(w^{-1}uw)(\alpha(w^{-1})(x))\\
	&= \alpha\circ i^{-1}_w(u)(\psi(x)).
\end{align*}

\begin{defn}
	Soit $\Gamma$ un graphe, la \emph{valence}\index{valence} d'un sommet $s$ est le nombre de arêtes reliant ce sommet, avec les boucles comptées deux fois.
\end{defn}

\begin{exmp}
	Considérons le graphe suivant
	\begin{center}
		\begin{tikzpicture}
		\draw (0,1.5) node [left] {$\Gamma$}
		(0,0) node [below] {$s$};
		
		\draw (0,0)--(0, 1.5);
		\draw (0,0)--(-1.06, -1.06);
		\draw (0,0)--(1.06, -1.06);
		
		\filldraw (0, 1.5) circle (1.5pt)
		(-1.06, -1.06) circle (1.5pt)
		(1.06, -1.06) circle (1.5pt)
		(0, 0) circle (1.5pt);
		\end{tikzpicture}
	\end{center}
	La valence de $s$ est $3$.
\end{exmp}

\begin{rem}
	L'action de $\Aut(F_N)$ descend à $\Out(F_N) = \Aut(F_N)/\Inn(F_N)$ et justifie à posteriori la définition de $F_N \curvearrowright \widetilde{\Gamma}$.
\end{rem}

\begin{rem}
	Soit $F_N \curvearrowright T$ une action minimale libre par isomorphismes, alors $T/F_N = \Gamma$ est un graphe fini avec $\pi_1(\Gamma) = F_N$ sans sommets de valence $1$. Et $\Gamma = \core(\Gamma)$ implique que $\Gamma$ est fini.
	
	Autrement dit $T$ possède un nombre fini d'orbites d'arrêtes.
\end{rem}

\begin{defn}
	Soit $\Gamma$ un graphe, on dit que $\Gamma$ est un \emph{graphe métrique}\index{graphe métrique} si chaque arrête a une longueur plus grand que $0$. On définit la \emph{longueur d'un chemin} \index{longueur d'un chemin} comme
	\[
		l(\gamma) = \sum_{i=1}^{n} l(e_i) \qquad \qquad \qquad \text{où } \gamma(e_1,\ldots,e_n).
	\]
	On définit aussi $d(v,w) = l([v,w])$.
\end{defn}

\begin{rem}
	Soit $F_N \curvearrowright T$, la longueur sur les arrêtes est $F_N$ invariante, c'est-à-dire, ce sont des longueur sur $T/F_N$.
\end{rem}

\begin{defn}[Culler-Vogtmann]
	Nous définissons l'\emph{outre espace} \index{outre espace} $\CV_N$ comme l'espace des arbre simpliciaux métriques sans sommets de valence $2$, avec une action de $F_N$ minimale libre par isométries; où $T = T'$ si et seulement s'il y a une isométrie $T \xrightarrow{\sim} T'$, $F_N$-invariante.
\end{defn}

\begin{defn}
	Soit $G \curvearrowright T$ par isométrie minimale, nous définissons la \emph{fonction longueur} \index{fontion longueur} comme
	\begin{align*}
		G &\longrightarrow \real_+\\
		g &\longmapsto \|g\|_T.
	\end{align*}
\end{defn}

\begin{thm}[Culler-Morgan]
	Soit $T$ un arbre métrique simplicial, $G \curvearrowright T$ par isométries minimales, $T$ et $G \curvearrowright T$ sont complètement déterminés par la fonction longueur
	\begin{align*}
		\CV_N &\xhookrightarrow{\quad\quad\quad} \real_+^{F_N}\\
		T &\xmapsto{\quad\quad\quad} \|\;\;\|_T
	\end{align*}
	ce qui fait de l'outre espace un espace topologique. Autrement dit, soient $T, T'$ deux arbres simpliciaux métriques avec une action de $F_N$ par isométries. Si pour tout $g \in F_N$, $\|g\|_T = \|g\|_{T'}$, alors $T =T'$.
\end{thm}

\begin{proof}
	Soient $f,g$ hyperboliques, $d(\Axe(f),\Axe(g)) = \frac{1}{2}\left(\|f\|_T\|g\|_T-\|f\|_T-\|g\|_T\right)$
	\begin{center}
		\begin{tikzpicture}
			\draw plot [smooth, tension=0.7] coordinates{(-2.5,0.5) (-1.5,0.75) (0,0.85) (1.5,0.75) (2.5,0.5)};
			\draw plot [smooth, tension=0.7] coordinates{(-2.5,-1.5) (-1.5,-1.25) (0,-1.15) (1.5,-1.25) (2.5,-1.5)};
			
			\draw[orange] (0,0.85)--(0,-1.15) node [right,midway] {$\leftarrow \frac{1}{2}\left(\|f\|_T\|g\|_T-\|f\|_T-\|g\|_T\right)$};
			\filldraw (0,0.85) circle (1.5pt)
			(0,-1.15) circle (1.5pt);
			\draw (2.5,0.5) node [right] {$\Axe_T(f)$}
			(2.5,-1.5) node [right] {$\Axe_T(g)$};
			
			
			\draw plot [smooth, tension=0.7] coordinates{(6.5,0.5) (7.5,0.75) (9,0.85) (10.5,0.75) (11.5,0.5)};
			\draw plot [smooth, tension=0.7] coordinates{(6.5,-1.5) (7.5,-1.25) (9,-1.15) (10.5,-1.25) (11.5,-1.5)};
			
			\draw[orange] (9,0.85)--(9,-1.15);
			\filldraw (9,0.85) circle (1.5pt)
			(9,-1.15) circle (1.5pt);
			\draw (11.5,0.5) node [right] {$\Axe_{T'}(f)$}
			(11.5,-1.5) node [right] {$\Axe_{T'}(g)$};
		\end{tikzpicture}
	\end{center}
	Donc, il y a une identification entre $T$ et $T'$ de $\Axe(f) \cup \Axe_T(g) \cup$points entre les deux. Et $\CV_N \subseteq \real_+^{F_N}$ fonctions longueurs des arbres.
\end{proof}

\begin{defn}
	Soit $G\curvearrowright T$ une action et $x \in T$, on définit la \emph{longueur de Lymdon \index{longueur de Lymdon}} comme
	\[
		g \longmapsto d(x,gx).
	\]
\end{defn}

\begin{defn}[Culler-Morgan, Chiswell]
	Soit $T$ un arbre, nous définissons les \emph{axiomes pour les longueurs de translations} \index{axiomes pour les longueurs de translations} comme
	\begin{align*}
		&1.) \;\|1\|_T = 0.\\
		&2.) \;|g\|_T = \|g^{-1}\|_T = \|hgh^{-1}\|_T.\\
		&3.) \;\|gh\|_T = \|gh^{-1}\|_T \text{, où } \max(\|gh\|_T,\|gh^{-1}\|_T) \le \|g\|_T + \|h\|_T.\\
		&4.) \|g\|_T > 0, \|h\|_T > 0.\\
		&5.) \|gh\|_T = \|gh^{-1}\|_T > \|g\|_T + \|g\|_T  \text{, où } \max(\|gh\|_T,\|gh^{-1}\|_T) = \|g\|_T + \|h\|_T.
	\end{align*}
	Une fonction qui vérifie 1,2,3 et 4 provient nécessairement de $G \curvearrowright T$, avec $T$ un arbre réel. Ce sont des conditions fermées.
\end{defn}


