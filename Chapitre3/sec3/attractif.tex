Considérons $F_3 = F_\mathcal{A}$ où $\mathcal{A} = \{ a,b,c \}$ et $\varphi \in \Aut(F_\mathcal{A})$, $\varphi \in \Phi \in \Out(F_\mathcal{A})$ donné par
\begin{align*}
	\varphi: a &\longmapsto c^{-1}b \\
	b &\longmapsto a\\
	c &\longmapsto cb^{-1}cb^{-1}c.
\end{align*}
Considérons aussi $T_\mathcal{A}$ l'arbre de Cayley de $F_\mathcal{A}$,
\begin{center}
	\begin{tikzpicture}
		\draw l-system [l-system={cayley3, axiom=[F] [+F] [-F] [++F] [--F] [+++F] , angle=60, step=3cm, order=2}];
		
		\draw (1.5,0) node [above] {$a$}
		(0.75,1.299) node [left] {$b$}
		(-0.75,1.299) node [left] {$c$};
		
		\draw (-3,3) node [above] {$T_\mathcal{A}$};
		
		\draw [->-=.5] (0,0)--(3,0);
		\draw [->-=.5] (0,0)--(1.5,2.5980);
		\draw [->-=.5] (0,0)--(-1.5,2.5980);
	\end{tikzpicture}
\end{center}

Soit $w \in F_\mathcal{A}$, nous avons $\|w\|_{T_\mathcal{A}} = \|w\|_\mathcal{A}$. La fonction longueur de translation détermine completement $F_N \curvearrowright T_\mathcal{A}$
\begin{align*}
	F_N &\longrightarrow \nat \\
	w &\longmapsto \|w\|_\mathcal{A}.
\end{align*}

$\Phi(T_\mathcal{A})$ est un arbre avec l'action de $F_\mathcal{A}$ par isométries. L'arbre ne change pas et pour tout $w \in F_\mathcal{A}$
\[
	\|w\|_{\Phi(T_\mathcal{A})} = \|\varphi(w)\|_{T_\mathcal{A}}
\]
Remarquez que cette longueur ne dépend pas de $\varphi \in \Phi$ mais seulement de $\Phi$. En effet, soient $\varphi,\varphi' \in \Phi$, alors il existe $u \in F_\mathcal{A}$ tel que $\varphi' = \varphi \circ i_u$, et donc
\begin{align*}
	\|\varphi'(w)\|_{T_\mathcal{A}} &= \|\varphi'(w)\|_{\mathcal{A}} =\|\varphi(i_u(w))\|_{\mathcal{A}} =\|\varphi(uwu^{-1})\|_{\mathcal{A}}\\
	&= \|\varphi(u)\varphi(w)\varphi(u)^{-1}\|_{\mathcal{A}} = \|\varphi(w)\|_{\mathcal{A}} = \|\varphi(w)\|_{T_\mathcal{A}}
\end{align*}

Prenons maintenant la matrice
\[
	M = \begin{pmatrix}
	0 & 1 & 0\\
	1 & 0 & 2\\
	1 & 0 & 3
	\end{pmatrix}
\]
associe à $\varphi$. Soit $\lambda$ la valeur propre dominante de $M$ [Perron-Frobenius], les coefficients de $M^3$ sont plus grands que $0$, donc $M$ admet une valeur propre de modele maximal qui est réelle et plus grand que $0$, le sous-espace associé est de dimension $1$ et il y a un vecteur propre à coordonnes plus grandes que $0$. En plus, $\lambda^3 -3\lambda^2 -\lambda +1 =0$ alors $\lambda \approx 3,2143$.

La fonction longueur est définie par la limite
\[
	\lim_{n \to +\infty} \frac{\|\varphi^n(w)\|_\mathcal{A}}{\lambda^n} = \|w\|_{T_\mathcal{A}}.
\]

Si dans le calcul de $\varphi(w)$ il n'y a pas de simplification, alors
\[
	(|\varphi(w)|_a)_{a \in \mathcal{A}} = M(|w|_a)_{a\in \mathcal{A}},
\]
où $|w|_a$ est le nombre d'occurrences de $a$ et $a^{-a}$ dans $w$. Cette limite existe donc toujours.

\begin{defn}
	La \emph{fonction longueur limite}\index{fonction longueur limite} est définie par
	\[
		\|w\|_{T_\Phi} = \lim_{n \to +\infty} \frac{\|w\|_{\Phi^{-n}(T_{\mathcal{A}})}}{\lambda^n}.
	\]
\end{defn}

Dans cet exemple $\|\;\;\;\|_{T_\Phi}$ n'est pas triviale. En effet $\varphi$ est \emph{train-track} \index{train track}, le calcul de $\varphi^n(a)$ ne nécessite pas aucune simplification.
\[
	\begin{tikzcd}
		a \ar[r] & c^{-1} b \ar[r] & c^{-1} bc^{-1} bc^{-1}.a & & \\
		& & & b.c^{-1} \ar[r] & a.c^{-1} \ar[l,bend left]\\
		c^{-1}.b \ar[r] & c^{-1}.a \ar[r] & c^{-1}.c^{-1}.a \ar[uu] \ar[loop below, distance=2em] & & 
	\end{tikzcd}
\]
On peut faire le même pour $\varphi^n(b)$ et $\varphi^n(c)$.

\begin{defn}
	On définit la \emph{fonction longueur de translation d'un arbre réel} \index{fonction longueur de translation d'un arbre réel} $T_\Phi$ l'\emph{arbre attractif}\index{arbre attractif} de $\Phi$ comme $\|\;\;\;\|_{T_\Phi}$.
\end{defn}

On définit $f:T_\mathcal{A} \to T_\mathcal{A}$ continue qui représente $\varphi$. $T_\mathcal{A}$ est un espace métrique en réalisant les arrêtes comme segments isométriques à $[0,1]$.

Notons que $f$ envoi de manière affine l'arrête $(1,a)$ sur le chemin $[1,c^{-1}b]$

\begin{center}
	\begin{tikzpicture}
		\filldraw (0,2) circle (1.5pt) node [left] {$1$}
		(3,2) circle (1.5pt) node [above] {$x$}
		(4,2) circle (1.5pt)
		(0,0) circle (1.5pt) node [left] {$1$}
		(4,0) circle (1.5pt)
		(5,0) circle (1.5pt) node [below] {$2x$}
		(8,0) circle (1.5pt);
		
		\draw [->-=.5] (0,2)--(3,2) node [above, midway] {$a$};
		\draw (3,2)--(4,2);
		
		\draw [->-=.5] (4,0)--(0,0) node [below, midway] {$c$};
		\draw (4,0)--(5,0);
		\draw [->-=.5] (5,0)--(8,0) node [below, midway] {$b$};
		
		\draw [->](0,1.9)--(0,0.1);
		\draw [->](3,1.9)--(5,0.1);
		\draw [->](4,1.9)--(8,0.1);
		
		\draw (4,-0.3) node [below] {« affine »};
	\end{tikzpicture}
\end{center}

On prolonge $f$ de sorte que $f(v) = \varphi(v)$ si $v \in F_\mathcal{A}$ est un sommet et que $f$ soit
\[
	f(u.x) = \varphi(u).f(x)
\]
$f$ est un relevé de
\begin{center}
	\begin{tikzpicture}
		\draw[->-=.5] (0,0) .. controls (-1.5,2) and (1.5,2) .. node [above] {$a$} (0,0);
		\draw[->-=.5] (0,0) .. controls (-2.48205, 0.299038) and (-0.982051, -2.29904) .. node [left] {$b$} (0,0);
		\draw[->-=.5] (0,0) .. controls (2.48205, 0.299038) and (0.982051, -2.29904) .. node [right] {$c$} (0,0);
		
		\draw[->] (2,0.3)--(6,0.3) node [above, midway] {$\overline{f}$};
		
		\draw[->-=.5] (8,0) .. controls (6.5,2) and (9.5,2) .. node [above] {$a$} (8,0);
		\draw[->-=.5] (8,0) .. controls (7.017949, -2.29904) and (5.51795, 0.299038) .. node [left] {$c$} (8,0);
		\draw[->-=.5] (8,0) .. controls (10.48205, 0.299038) and (8.982051, -2.29904) .. node [right] {$b$} (8,0);
	\end{tikzpicture}
\end{center}
au revêtement universel $T_\mathcal{A}$. Pour deux point $x,y \in T_\mathcal{A}$
\begin{align*}
	d_n(x,y) = d(f^n(x), f^n(y)),\\
	d_\infty = \lim_{n \to +\infty} \frac{d_n}{\lambda^n}.
\end{align*}
Cette limite existe et $d_\infty(1,a) \neq 0$. $(T_\mathcal{A},d_n)$ est un espace pseudo-métrique. $(T_\mathcal{A}/\sim,d_\infty)$ où $x\sim y$ si $d_\infty (x,y) = 0$ est un espace métrique. $T_\Phi$ c'est l'arbre attractif.

\begin{rem}
	Remarquons que
	\begin{align*}
		d_1(\mu x, \mu y) &= d(f(\mu x), f(\mu y)) = d(\varphi(u)f(x), \varphi(u)f(y))\\ 
		&= d(f(x), f(y)) = d_1(x,y).
	\end{align*}
\end{rem}



