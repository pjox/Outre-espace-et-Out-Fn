\begin{defn}
	Soit $T$ un arbre, on dit que $T$ est \emph{géodésique} \index{arbre géodésique} si pour tous $x,y \in T$, il y a un chemin isométrique de $x$ à $y$.
	\begin{center}
		\begin{tikzpicture}
			\draw (0,0)--(-2,2);
			\draw (0,0)--(2,2);
			\filldraw (-2,2) circle (1.5pt) node [left] {$x$}
			(2,2) circle (1.5pt) node [right] {$y$};
			
			\draw (-7,1) node {$\gamma:[0,d(x,y)]$};
			\draw [->] (-5.5,1)--(-1.5,1) node [below, midway] {isométrie};
		\end{tikzpicture}
	\end{center}
\end{defn}

\begin{defn}
	Soit $T$ un arbre, on dit que $T$ est \emph{$0$-hyperbolique} \index{arbre $0$-hyperbolique} si les tripodes ont un centre
	\begin{center}
		\begin{tikzpicture}
			\draw (0,0)--(0, -1.5);
			\draw (0,0)--(-1.06, 1.06);
			\draw (0,0)--(1.06, 1.06);
			
			\filldraw (0, -1.5) circle (1.5pt) node [below] {$z$}
			(-1.06, 1.06) circle (1.5pt) node [left] {$x$}
			(1.06, 1.06) circle (1.5pt) node [right] {$y$}
			(0, 0) circle (1.5pt) node [right] {$w$};
		\end{tikzpicture}
	\end{center}
	où le produit de Gromov $\langle y,z \rangle_x = \frac{1}{2}\left(d(x,y)+d(x,z)-d(y,z) \right)$
\end{defn}

\begin{rem}
	Dans la figure précédente, $w$ est le point de $[x,y]$ à distance $\langle y,z \rangle_x$. On peut montrer que $w \in  [x,z] \cap [y,z]$ et que $\{w \} = [x,y] \cap [x,z] \cap [y,z]$. Si $[x,y]$ et $y,z$ n'ont que le point $y$ en commun, alors $[x,y] \cup [y,z] = [x,z]$.
\end{rem}

\begin{defn}
	Un \emph{arbre réel}\index{arbre réel} est une espace métrique $(T,d)$, géodésique, $0$-hyperbolique et connexe par arcs.
\end{defn}

\begin{exmp}
	Considérons la \emph{distance sncf}\index{distance sncf} sur $\real^2$ défini par
	\[
		d(A,B) = \begin{dcases*}
			OA+OB & sinon,\\
			AB & si $A,O$ et $B$ sont alignés. 
		\end{dcases*}
	\]
	Celui-ci est un arbre reél avec un point de branchement $O$ de valence $2^\aleph_0$.
\end{exmp}

\begin{exmp}
	Considérons la \emph{métrique du peine}\index{métrique du peine} sur $\real^2$ défini par
	\[
		d(A,B) = \begin{dcases*}
			|y_A|+|y_B|+|x_B-x_A| & ou,\\
			|y_A - y_B|& si $x_A=x_B$. 
		\end{dcases*}
	\]
	Ici, tous les point de l'axe des abscisses sont des points de branchement de valence $4$.
\end{exmp}

\begin{axio}
	Soit $X$ un arbre réel et $d:X \to \real_+$ une distance alors
	\begin{enumerate}
		\item[(0)] Si $d(x,y) \neq 0$ alors $x \neq y$.
		\item[(i)] $d(x,x) = 0$.
		\item[(ii)] $d(x,y) = d(y,x)$.
		\item[(iii)] $d(x,z) = d(x,y) + d(y,z)$.
		\item[(iv)] $d(x,y) + d(z,t) \le \max\{d(x,z) + d(y,t); d(x,t)+d(y,z) \} + \delta$. ($\delta-$hyperbolique). Parmi ces deux nombres, les deux plus grands sont égaux.
	\end{enumerate}
\end{axio}

\begin{lem}
	Les géodésiques sont uniques.
\end{lem}

\begin{proof}
	\
	\begin{center}
		\begin{tikzpicture}
		\draw (0,0)--(2,0)
		(0,0)--(-1,-1)
		(0,0)--(-1,1)
		(2,0)--(3,1)
		(2,0)--(3,-1);
		
		\draw (-1,-1) node [left] {$x$}
		(-1,1) node [left] {$y$}
		(3,-1) node [right] {$t$}
		(3,1) node [right] {$z$};
		
		\filldraw (0,0) circle (1.5pt)
		(2,0) circle (1.5pt);
		
		\draw [orange] plot [smooth, tension=0.7] coordinates{(5,0) (5.5,0.5) (6,0.5) (6.5,1) (7,0.5) (8,0)};
		\draw [blue] plot [smooth, tension=0.7] coordinates{(5,0) (5.5,-0.5) (6,0) (6.5,0.5) (7,1) (8,0)};
		
		\draw [orange] (5.5,0.4)--(5.5,0.6) node [above] {$z$};
		\draw [blue] (5.5,-0.6)--(5.5,-0.4) node [below, near start] {$t$};
		
		\filldraw (5,0) circle (1.5pt) node[left] {$x$}
		(8,0) circle (1.5pt) node[right] {$y$};
		\end{tikzpicture}
	\end{center}
	Soient $\gamma, \gamma'$ deux géodésiques entre $x$ et $y$, $z \in \gamma$ et $t \in \gamma'$. Si $d(x,t) = d(x,z)$ alors $t = z$. Sinon
	\[
		d(x,y) = d(x,z)+d(y,z) = d(x,t) + d(z,t),
	\]
	et d'après (iv),
	\[
		d(x,y)+d(z,t) \le \max \{d(x,y); d'(x,y) \}
	\]
	or
	\[
		d(x,z)+d(y,t) = d(x,z) + d(z,y) = d(x,y)
	\]
	Donc $d(z,t) = 0$.
\end{proof}

\begin{prop}
	Soit $T$ un arbre réel et $f:T \to T$ une isométrie, $\|f\|_T = \inf\{d(x,f(x)) \mid x\in T \}$.
	\begin{enumerate}
		\item La longueur de translation est atteinte.
		\item Si $\|f\|_T > 0$ alors $\Axe(f) = \{x \mid d(x,f(x)) = \|f\|_T\}$ est isométrique à $\real$ et $f$ agit sur $\Axe(f)$ par translation $\|f\|_T$.
		\item Si $\|f\|_T = 0$ alors $\Fix(f)$ est un sous arbre fermé non-vide de $T$.
	\end{enumerate}
\end{prop}

\begin{proof}
	Pour tout $x \in T$, $l = d(x,f^2(x)) - d(x,f(x))$
	\begin{center}
		\begin{tikzpicture}
			\draw (-3,0)--(0,0);
			\draw (0,0)--(3,0) node [above, midway] {$l$};
			
			\draw (-3,0)--(-3,1.5) node [left, midway] {$\delta$};
			\draw (0,0)--(0,1.5) node [right,midway] {$\delta$};
			\draw (3,0)--(3,1.5);
			
			\draw [blue](-2.95,0.05)--(-2.95,1.5);
			\draw [blue](-2.95,0.05)--(-0.05,0.05);
			\draw [blue](-0.05,0.05)--(-0.05,1.5);
			
			\draw [blue](0.05,0.05)--(0.05,1.5);
			\draw [blue](0.05,0.05)--(2.95,0.05);
			\draw [blue](2.95,0.05)--(2.95,1.5);
			
			\draw [orange](-3,-0.05)--(3,-0.05);
			
			
			\filldraw (-3,1.5)circle (1.5pt) node [above] {$x$}
			(0,1.5) circle (1.5pt) node [above] {$f(x)$}
			(3,1.5) circle (1.5pt) node [above] {$f^2(x)$};
			
			\filldraw (-3,0)circle (1.5pt) node [below] {$y$}
			(0,0) circle (1.5pt) node [below] {$f(y)$}
			(3,0) circle (1.5pt) node [below] {$f^2(y)$};
		\end{tikzpicture}
	\end{center}
	Soit $y$ le point de $[x;f(x)]$ à distance $\frac{1}{2}\left( d(x,f(x)-l) \right) - \delta$. Les segments $[y;f(y)]$ et $[f(y),f^2(y)]$ n'ont que $f(y)$ en commun. $y$, $f(y)$, $f^2(y)$ sont alignés donc $f(y)$, $f^2(y)$, $f^3(y)$ sont alignés et $d(y,f(y)) = l$. Soit
	\[
		A = \bigcup_{n \in \inte} [f^n(y); f^{n+1}(y)].
	\]
	$A$ est isométrique à $\real$ et $f$ agit par translation sur $A$ de longueur $l$. Alors pour tout $z \in T$ $d(z,f(z)) = 2d(z,A)+l \ge l$, donc $l = \|f\|_T$ et $\Axe(f) = A$.
	
	Cas $l = 0$: donc $f(y) = y$. En effet
	\[
		d(x,y) = \langle f(x) ,f^2(x) \rangle_x = d(f(x) ,y)
	\]
	donc $y$ est le centre du tripode et $f(y) = y$. De plus $\Fix(f)$ est convexe et fermé.
\end{proof}

Maintenant, nous nous demandons comment détecter que $x$ est sur l'$\Axe(f)$ ? $x \in \Axe(f)$ si et seulement si $\delta = 0$ si et seulement si $[x;f(x)]\cap [f(x);f^2(x)] = \{f(x)\}$

\begin{center}
	\begin{tikzpicture}
	\draw (-3,0)--(0,0);
	\draw (0,0)--(3,0);
	
	\draw (-3,0)--(-3,1.5);
	\draw (0,0)--(0,1.5) node [right,midway] {$\delta$};
	\draw (3,0)--(3,1.5);
	
	
	\filldraw (-3,1.5)circle (1.5pt) node [above] {$x$}
	(0,1.5) circle (1.5pt) node [above] {$f(x)$}
	(3,1.5) circle (1.5pt) node [above] {$f^2(x)$};
	\end{tikzpicture}
\end{center}

\begin{defn}
	Dans un arbre réel $T$ une \emph{direction}\index{direction} basée au point $x$ connexe de $T - \{x\}$.
\end{defn}

Ce définition est équivalente avec de ses germes sortants de $x$. Une direction $d$ basée en $x$ est donc dans l'$\Axe(f)$. C'est-à-dire que $x \in \Axe(f)$ et que $d \cap \Axe(f) \neq 0$.

En fait $d\cap \Axe(f)$ est un demi-axe ouvert limité par $x$.
\begin{center}
	\begin{tikzpicture}
	\draw (-3,0)--(0,0);
	\draw (0,0)--(3,0);
	
	\draw[->, blue] (-3,0.75)--(-3,1.5) node [right] {$d$};
	\draw[->, blue] (0,0.75)--(0,1.5) node [right] {$f(d)$};
	\draw[->, orange] (-3,0.75)--(-3,0) node [right,midway] {$d'$};
	\draw[->, orange] (0,0.75)--(0,0) node [right,midway] {$f(d')$};
	\draw (3,0)--(3,1.5);
	
	
	\filldraw (-3,0.75)circle (1.5pt) node [left] {$x$}
	(0,0.75) circle (1.5pt) node [left] {$f(x)$}
	(3,0.75) circle (1.5pt) node [right] {$f^2(x)$};
	
	\draw[->, blue] (6,0.75)--(5,0.75) node [above] {$d$};
	\draw[->, orange] (6,0.75)--(7,0.75) node [above] {$d'$};
	\draw (7,0.75)--(9,0.75);
	\draw[->, blue] (10,0.75)--(9,0.75) node [above] {$f(d)$};
	\draw[->, orange] (10,0.75)--(11,0.75) node [above] {$f(d')$};
	
	\filldraw (6,0.75)circle (1.5pt) node [below] {$x$}
	(10,0.75) circle (1.5pt) node [below] {$f(x)$};
	\end{tikzpicture}
\end{center}

la direction est dans l'axe de $f$ si $f(d) \subseteq d$ ou $d \subseteq f(d)$; elle n'est pas dans l'axe si $d\cup f(d) = T$ ou $d \cap f(d) = \emptyset$.

\begin{thm}[Rips, 1991; Gavoriauv-Levit]
	$G$ de t.$f$ qui agit librement par isométrie sur l'arbre réel, alors $G$ est un produit libre de groupes libres, de groupes de surfaces et de $\inte^n$ (sauf $\pplane[1]$).
\end{thm}

\begin{exmp}
	$\inte^n$ agit sur $\real$ par translation; soient $\alpha_1, \ldots, \alpha_n \in \real$ linéairement indépendants sur $\rat$. Soit $\overline{a} = (a_1,\ldots, a_n) \in \inte^n$ et $x \in \real$.
	\[
		\overline{a}\cdot x = x + \sum_{i=1}^{n} a_i\cdot \alpha_i
	\]
	est une action libre car $\alpha_1, \ldots, \alpha_n$ linéairement indépendants.
\end{exmp}

Revenons à l'outre espace. Soit $F_N \curvearrowright T$ minimale par isométries
\begin{align*}
	\|\;\;\;\|_T: F_N &\longrightarrow \real_+\\
	w &\longmapsto \|w\|_T \qquad \CV_N \subseteq (\real_+)^{F_N}.
\end{align*}

\begin{defn}
	Soit $X$ un espace topologique, nous définissons la \emph{distance de Gromov-Hausdorff} \index{distance de Gromov-Hausdorff} entre deux compactes métriques $K$ et $K'$ comme
	\[
		d(K,K') = \inf_X d_x(K,K')
	\]
	où $K \xhookrightarrow{\text{isométrie}} X$ et $K' \xhookrightarrow{\text{isométrie}} X$
\end{defn}

\begin{defn}
	Soit $X$ un espace, on définit la \emph{topologie de Gromov-Hausdorff} \index{topologie de Gromov-Hausdorff} équivariante par la distance
	\[
		d(K,K') = \max \left( \sup_{x \in K} \inf_{y\in K'} d(x,y) ; \sup_{x \in K'} \inf_{y\in K} d(x,y) \right)
	\]
\end{defn}

\begin{thm}[Paulin]
	Soit $(T_n)_{n\in \nat}$ une famille de $T$ arbres réels avec une action $G$ par isométries, $T_n \xrightarrow{n \to +\infty} T$ dans la topologie de Gromov-Hausdorff équivariante, si pour toute partie fini $A$ de $T$, toute partie fini $W$ de $G$ et toute $\varepsilon > 0$; existe un $N$ tel que pour tout $n \ge N$ existe $i_n:A \to T_n$ telle que pour tous $x,y \in A$ et tous $u,v \in W$
	\[
		\left|d_{T_n}(ui_n(x); vi_n(u) - d_T(ux; vy) \right| \le \varepsilon.
	\]
\end{thm}

\begin{lem}
	Si $T_n \xrightarrow[n \to +\infty]{} T$ au sens de Paulin-Gromov-Hausdorff, alors $\|\;\;\;\|_{T_N} \xrightarrow{n \to +\infty} \|\;\;\;\|_T$.
\end{lem}

\begin{proof}
	Soit $w \in G$, soit $x \in T$, $A = \{x\}$ et $W = \{w,w^2\}$. Pour tout $\varepsilon > 0$ existe un $N$ tel que pour tout $n \ge N$, existe $x_n \in T_n$ tel que
	\begin{align*}
		\|w\|_{T_n} &= d_{T_n}(x_n, w^2x_n) - d_{T_n}(x_n, w^x_n)\\
		\|w\|_{T} &= d(x, w^2x) - d(x, w^x).
	\end{align*}
	Alors
	\[
		\left|\|w\|_{T_n} - \|w\|_{T} \right| \le 2 \varepsilon.
	\]
\end{proof}

\begin{defn}
	Nous disons que $F_N \curvearrowright T$ est \emph{minimale} \index{action minimale}, si la fonction longueur ne dit rien en dehors des axes.
\end{defn}

\begin{defn}
	Nous disons que $F_N \curvearrowright T$ par isométries est \emph{très petite} \index{action très petite} si:
	\begin{enumerate}
		\item[(i)] Les stabilisateurs des tripodes sont triviaux
		\[
			\Stab(x) \cap \Stab(y) \cap \Stab(z) = \{1\}
		\]
		où $x,y,z \in T$ est un tripode.
		
		\item [(ii)] Les stabilisateurs des arcs (non-triviaux) sont triviaux ou cycliques axiaux.
	\end{enumerate}
\end{defn}

\begin{prop}
	$\overline{\CV_N}$ est constitué des arbres réels avec une action minimale très petite par isométrie de $F_N$.
\end{prop}

\begin{proof}
	Soit $T_n \in \CV_N$, $T_n \to T$, $x,y,z \in T$ un tripode, et $g \in F_N$ qui le fixe. Soit $A\{x,y,z\}$ et $W = \{g\}$, alors pour tout $\varepsilon > 0$ existe un $N$ tel que pour tout $n \ge N$ existent $x_n.y_n,z_n \in T_n$ tels que
	\[
		d(x_n, gx_n)\le \varepsilon \qquad d(y_n, gy_n)\le \varepsilon \qquad d(z_n, gz_n)\le \varepsilon.
	\]
	\begin{center}
		\begin{tikzpicture}
		\draw (0,0)--(0, -1.5);
		\draw (0,0)--(-1.06, 1.06);
		\draw (0,0)--(1.06, 1.06);
		
		\draw [->] (0.2, -1.2) to [out=0,in=0,looseness=8] node[right,midway] {$g$} (0.2, -1.5);
		
		\filldraw (0, -1.5) circle (1.5pt) node [below] {$z$}
		(-1.06, 1.06) circle (1.5pt) node [left] {$x$}
		(1.06, 1.06) circle (1.5pt) node [right] {$y$}
		(0, 0) circle (1.5pt) node [right] {$w$};
		\end{tikzpicture}
	\end{center}
En prenant
\[
	\varepsilon < \frac{1}{2} \min \left(\langle y,z \rangle_x; \langle x,y \rangle_x; \langle x,z \rangle_x \right),
\]
alors $x_n,y_n,z_n$ est un tripode et son centre $w_n$ est aussi le centre de $gx_n,gy_n,gz_n$, donc $gw_n = w_n$.

Or $T_n \in \CV_N$ et $F_N \curvearrowright T_n$ est libre, donc $g = 1$.

De même, soit $x \neq y$ et $g,h \in F_N$ tels que $gx = x$, $gy = y$, $hx = x$ et $hy = y$; et $g \neq 1$ et $h \neq 1$. Soit $\varepsilon < \frac{1}{3} d(x,y)$

\begin{center}
	\begin{tikzpicture}
	\draw (0,0)--(-1,-1)
	(0,0)--(-1,1)
	(2,0)--(3,1)
	(2,0)--(3,-1)
	(-0.5,0.5)--(0,1)
	(2.5,0.5)--(2,1);
	
	
	\draw [orange] (0,0)--(2,0);
	\draw (-2.5,0) node {$T_n$};
	
	\draw (-1,-1) node [left] {$gx_n$}
	(-1,1) node [left] {$x_n$}
	(3,-1) node [right] {$gy_n$}
	(3,1) node [right] {$y_n$};
	
	\filldraw (0,0) circle (1.5pt) node [below right] {$z_n$}
	(2,0) circle (1.5pt) node [below] {$t_n$}
	(-1,-1) circle (1.5pt)
	(-1,1) circle (1.5pt)
	(3,1) circle (1.5pt)
	(3,-1) circle (1.5pt)
	(0,1) circle (1.5pt) node [above] {$hx_n$}
	(2,1) circle (1.5pt) node [above] {$hy_n$};
	
	\draw (0,2.5)--(2,2.5);
	\draw (-2.5,2.5) node {$T$};
	
	\filldraw (0,2.5) circle (1.5pt) node [left] {$x$}
	(2,2.5) circle (1.5pt) node [right] {$y$};
	\end{tikzpicture}
\end{center}

$g$ agit hyperboliquement sur $T_n$ et $\Axe(g) \cap [x_n,gx_n] \neq 0$ donc $[z_n,t_n] \subset \Axe(g)$. De plus $\|g\|_{T_n} \le \varepsilon$ et $d(z_n,t_n) \ge d(x,y) - z_\varepsilon$. De même pour $h$, $\|h\|_{T_n} \le \varepsilon$. $|\Axe_{T_n}(g) \cap \Axe_{T_n}(h)| \ge d(x,y) - 2\varepsilon$.

Soit $a$ au milieu de $\Axe(g) \cap \Axe(h) = I$, alors $ga \in I$, $hga \in I$, $g^{-1}hga \in I$ et $h^{-1}g^{-1}hga \in I$. (Si $\|g\|_{T_n} \le \frac{1}{4}l(I)$ et $\|h_{T_n} \le \frac{1}{4} l(I)$).

Mais comme ces quatre sont sur les axes où $g$ et $h$ agissent par translation, ça commute, donc $h^{-1}g^{-1}hga = a$ et comme l'action est libre $h^{-1}g^{-1}hg = 1$ si et seulement si $hg = gh$.
\begin{center}
	\begin{tikzpicture}
		\draw[blue](0,0.01)--(2,0.01);
		\draw[blue] (2,0.01)--(3,1.01);
		\draw[blue] (0,0.01) .. controls (-0.5, 0.01) and (-1.1, 0.01) .. (-1.01,1.01);
		
		\draw[orange](0,-0.01)--(2,-0.01);
		\draw[orange] (2,-0.01)--(3,-1.01);
		\draw[orange] (0,-0.01) .. controls (-0.5, -0.01) and (-1.1, -0.01) .. (-1.01,-1.01);
		
		\draw [blue] (-1.1,1.01) node [left] {$\Axe(g)$};
		\draw [orange] (-1.1,-1.01) node [left] {$\Axe(g)$};
		
		\filldraw (-0.3,0) circle (1.5pt) node [below] {$a$};
		\filldraw (0.7,0) circle (1.5pt) node [below] {$ga$};
		\filldraw (1.7,0) circle (1.5pt) node [below] {$hga$};
		
		\draw [->-=0.5] (-0.2,0.3)--(0.6,0.3) node [above,midway] {$g$};
		\draw [->-=0.5] (0.8,0.3)--(1.6,0.3) node [above,midway] {$h$};
	\end{tikzpicture}
\end{center}
Dans le groupe libre, les sous-groupes commutatifs sont cycliques. Existe $u \in F_N$, $g = u^\alpha$ et $h = u^\beta$. $u$ fixe aussi $[x,y]$ de $T$.
\end{proof}

\begin{rem}
	Être très petit est une condition fermée
\end{rem}

\begin{rec}[Cohen-Lustig, Bestvina-Feighn]
	Tout $F_N \curvearrowright T$ très petite et minimal est limite d'arbres simpliciaux.
\end{rec}
